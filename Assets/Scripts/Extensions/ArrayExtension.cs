﻿using UnityEngine;

public static class ArrayExtension
{
    public static void DebugLog(this int[,] grid)
    {
        Debug.Log("Grid:");
        for (int y = grid.GetLength(1) - 1; y > -1; y--)
        {
            string s = "";
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                if (s.Length == 0)
                    s += grid[x, y].ToString();
                else
                    s += " ; " + grid[x, y].ToString();
            }
            Debug.Log(s);
        }
    }
}