﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtension
{
    public static List<TurnAction> FisherYatesShuffle(this List<TurnAction> list)
    {
        int j;
        TurnAction a;
        for (int i = list.Count - 1; i > 0; i--)
        {
            j = Random.Range(0, i + 1);
            a = list[i];
            list[i] = list[j];
            list[j] = a;
        }
        return list;
    }

    public static List<TurnAction> AddMultiple(this List<TurnAction> list, TurnAction action, int iterations)
    {
        for (int i = 0; i < iterations; i++)
        {
            list.Add(action);
        }
        return list;
    }
}