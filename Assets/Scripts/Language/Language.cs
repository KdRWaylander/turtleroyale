﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Language", menuName = "Language")]
public class Language : ScriptableObject
{
    public LanguageList LanguageType;

    [Header("Police sizes")]
    public int PoliceSize_Title;
    public int PoliceSize_Text;

    [Header("Configure panel")]
    public string Configure_InputFieldPlaceHolder_1;
    public string Configure_InputFieldPlaceHolder_2;
    public string Configure_InputFieldPlaceHolder_3;
    public string Configure_InputFieldPlaceHolder_4;

    [Header("How to play panel")]
    public string HowToPlay_Title;
    [TextArea(15, 20)]
    public string m_GameRules;

    [Header("Settings panel")]
    public string Settings_Title;
    public string Settings_Music;
    public string Settings_SFX;
    public string Settings_Language;

    [Header("Credits panel")]
    public string Credits_Title;
    [TextArea(5, 10)]
    public string Credits_Text;

    [Header("Buttons")]
    public string Button_Yes;
    public string Button_No;
    [TextArea(2, 5)]
    public string Button_HomeText;
    [TextArea(2, 5)]
    public string Button_RestartText;
    [TextArea(2, 5)]
    public string Button_QuitText;

    [Header("Actions panel")]
    public string Action_Place;
    public string Action_Move;
    public string Action_Jump;
    public string Action_Destroy;
    public string Action_Terrain;
    public string Action_Victory;
    public string Action_Turn;

    [Header("Toast")]
    public string Toast_Text;

    [Header("Tooltips")]
    public string Tooltips_Play;
    public string Tooltips_Configure;
    public string Tooltips_HowToPlay;
    public string Tooltips_Settings;
    public string Tooltips_Credits;
    public string Tooltips_Quit;
    public string Tooltips_Color;
    [TextArea(2, 5)]
    public string Tooltips_ChangeNumberOfPlayers;

    [Header("FirstTimePanel")]
    public string FirstTime_Title;
    [TextArea(5, 10)]
    public string FirstTime_Text;
    public string FirstTime_Language;
    public string FirstTime_Audio;
    public string FirstTime_Play;

    [Header("Other")]
    public string Loading;
    public string RemainingCards;
    public string Tooltips_AI;
    public string GameSpeed_Title;
    public string GameSpeed_Slow;
    public string GameSpeed_Normal;
    public string GameSpeed_Fast;
}