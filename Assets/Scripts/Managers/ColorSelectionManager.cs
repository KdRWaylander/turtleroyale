﻿using System.Collections.Generic;
using UnityEngine;

public class ColorSelectionManager : MonoBehaviour
{
    public static ColorSelectionManager Instance { get; private set; }
    public List<Color> ColorList { get; private set; }
    public List<Color> UsedColorsList { get; set; }

    [SerializeField] private Color[] m_BaseColors;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        ColorList = new List<Color>();
        UsedColorsList = new List<Color>();

        for (int i = 0; i < m_BaseColors.Length; i++)
        {
            ColorList.Add(m_BaseColors[i]);
        }

        foreach (PlayerColorSelector cs in GetComponentsInChildren<PlayerColorSelector>())
        {
            cs.ApplyDefaultColor();
        }
    }

    public Color GetNextColor(PlayerColorSelector demandingColorSelector)
    {
        int index = demandingColorSelector.CurrentColorIndex;
        bool correctColor = false;
        while (correctColor == false)
        {
            index = index + 1 >= ColorList.Count ? 0: index + 1;
            if (UsedColorsList.Contains(ColorList[index]) == false)
            {
                demandingColorSelector.CurrentColorIndex = index;
                correctColor = true;
            }
        }
        return ColorList[index];
    }

    public Color GetPreviousColor(PlayerColorSelector demandingColorSelector)
    {
        int index = demandingColorSelector.CurrentColorIndex;
        bool correctColor = false;
        while (correctColor == false)
        {
            index = index - 1 < 0 ? ColorList.Count - 1 : index - 1;
            if (UsedColorsList.Contains(ColorList[index]) == false)
            {
                demandingColorSelector.CurrentColorIndex = index;
                correctColor = true;
            }
        }
        return ColorList[index];
    }
}