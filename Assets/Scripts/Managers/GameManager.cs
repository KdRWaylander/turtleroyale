﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    private List<TurnAction> m_ActionsList;
    private int m_CurrentActionIndex;

    public TurnAction Action { get; private set; }
    public int MaxNumberOfPlayedActions { get; private set; }
    public bool IsGameOn { get; private set; }
    public bool IsGameOver { get; private set; }
    public int CurrentActionIndex { get { return m_CurrentActionIndex; } }

    public event Action<TurnAction, Player> NewActionDrawn;
    public event Action GameIsOver;

    private void Awake()
    {
        Instance = this;

        m_ActionsList = new List<TurnAction>();
        m_ActionsList.AddMultiple(TurnAction.Move, 3);
        m_ActionsList.AddMultiple(TurnAction.Jump, 1);
        m_ActionsList.AddMultiple(TurnAction.Destroy, 2);
        m_ActionsList.AddMultiple(TurnAction.Terrain, 2);
        m_ActionsList.FisherYatesShuffle();

        MaxNumberOfPlayedActions = 1;
    }

    private void Start()
    {
        Action = TurnAction.PlacePlayer;
    }

    public void StartGame()
    {
        IsGameOn = true;

        TurnManager.Instance.ResetTurns();

        List<Player> players = PlayerManager.Instance.GetPlayers();
    }

    public void DrawNewAction()
    {
        m_CurrentActionIndex += 1;
        if (m_CurrentActionIndex >= m_ActionsList.Count)
        {
            m_ActionsList.FisherYatesShuffle();
            m_CurrentActionIndex = 0;
        }

        Action = DrawFeasibleAction();
        if (Action == TurnAction.Terrain)
        {
            TerrainManager.Instance.ClearAlreadyMovedTerrainChunksList();
        }
    }

    private TurnAction DrawFeasibleAction()
    {
        TurnAction action = new TurnAction();

        bool isActionFeasible = false;
        while (isActionFeasible == false)
        {
            action = m_ActionsList[m_CurrentActionIndex];

            if (TerrainManager.Instance.CheckActionFeasibility(action))
            {
                isActionFeasible = true;
            }
            else
            {
                m_CurrentActionIndex += 1;

                if (m_CurrentActionIndex >= m_ActionsList.Count)
                {
                    m_CurrentActionIndex = 0;
                    m_ActionsList.FisherYatesShuffle();
                }
            }
        }

        return action;
    }

    public void GameOver()
    {
        IsGameOn = false;
        IsGameOver = true;

        HandleGameIsOver();
    }

    public void HandleNewActionDrawn(TurnAction action, Player player)
    {
        if (NewActionDrawn != null)
        {
            NewActionDrawn(action, player);
        }
    }

    public void HandleGameIsOver()
    {
        SettingsManager.Instance.PlayVictorySound();
        if (GameIsOver != null)
        {
            GameIsOver();
        }
    }
}

[Serializable]
public enum TurnAction
{
    PlacePlayer,
    Move,
    Jump,
    Destroy,
    Terrain
};