﻿using System;
using UnityEngine;

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager Instance { get; private set; }
    public Language CurrentLanguage { get; private set; }
    public LanguageList CurrentLanguageType { get; private set; }

    [SerializeField] private Language[] m_Languages;

    private const string LanguageKey = "LANGUAGE";
    private const string EnglishValue = "English";
    private const string FrenchValue = "French";

    public event Action<Language> NewLanguageSelected;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        CurrentLanguageType = GetLanguageListFromPlayerPrefs();
        CurrentLanguage = GetLanguageFromLanguageList(CurrentLanguageType);
    }

    public LanguageList GetLanguageListFromPlayerPrefs()
    {
        if (PlayerPrefs.HasKey(LanguageKey) == false)
            PlayerPrefs.SetString(LanguageKey, EnglishValue);

        string status = PlayerPrefs.GetString(LanguageKey);
        if (status == FrenchValue)
        {
            return LanguageList.French;
        }
        else
        {
            return LanguageList.English;
        }
    }

    public Language GetLanguageFromPlayerPrefs()
    {
        if (PlayerPrefs.HasKey(LanguageKey) == false)
            PlayerPrefs.SetString(LanguageKey, EnglishValue);

        string status = PlayerPrefs.GetString(LanguageKey);
        if (status == FrenchValue)
        {
            return GetLanguageFromLanguageList(LanguageList.French);
        }
        else
        {
            return GetLanguageFromLanguageList(LanguageList.English);
        }
    }

    private void SetLanguageInPlayerPrefs(LanguageList languageList)
    {
        if(PlayerPrefs.HasKey(LanguageKey) == false)
            PlayerPrefs.SetString(LanguageKey, EnglishValue);

        switch (languageList)
        {
            case LanguageList.English:
                PlayerPrefs.SetString(LanguageKey, EnglishValue);
                break;
            case LanguageList.French:
                PlayerPrefs.SetString(LanguageKey, FrenchValue);
                break;
        }
    }

    public Language GetLanguageFromLanguageList(LanguageList currentLanguageType)
    {
        foreach(Language language in m_Languages)
        {
            if(language.LanguageType == currentLanguageType)
            {
                return language;
            }
        }
        return m_Languages[0];
    }

    public void SetNewLanguage(LanguageList language)
    {
        CurrentLanguageType = language;
        CurrentLanguage = GetLanguageFromLanguageList(language);
        SetLanguageInPlayerPrefs(language);

        HandleNewLanguageSelected(CurrentLanguage);
    }
    
    public void HandleNewLanguageSelected(Language language)
    {
        if(NewLanguageSelected != null)
        {
            NewLanguageSelected(language);
        }
    }

    public void HandleNewLanguageSelected(LanguageList languageList)
    {
        if (NewLanguageSelected != null)
        {
            NewLanguageSelected(GetLanguageFromLanguageList(languageList));
        }
    }

    public void UpdateLanguage()
    {
        if (NewLanguageSelected != null)
        {
            NewLanguageSelected(CurrentLanguage);
        }
    }
}

[Serializable]
public enum LanguageList
{
    English,
    French
};