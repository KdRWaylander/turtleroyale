﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchManager : MonoBehaviour
{
    [SerializeField] private UILoadingBar m_LoadingBar;

    private void Start()
    {
        m_LoadingBar.LoadLevel();
    }
}