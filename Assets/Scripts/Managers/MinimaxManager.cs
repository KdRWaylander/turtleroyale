﻿using UnityEngine;
using System.Collections.Generic;

public class MinimaxManager : MonoBehaviour
{
    public static MinimaxManager Instance { get; private set; }
    public List<Vector3> m_Directions;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_Directions = new List<Vector3>() { Vector3.right, Vector3.forward, Vector3.left, Vector3.back };
    }

    public Vector3 Direction(Pawn pawn, int length)
    {
        int[,] grid = TerrainManager.Instance.Grid;
        int x = pawn.X;
        int z = pawn.Z;

        Vector3 dir = Vector3.zero;
        float bestEval = Mathf.NegativeInfinity;
        foreach(Vector3 direction in m_Directions)
        {
            if (TerrainManager.Instance.IsInsideBounds(x + (int)direction.x, z + (int)direction.z) && grid[x + (int)direction.x, z + (int)direction.z] == 0)
            {
                grid[x, z] = 0;
                Debug.Log("x: " + x);
                Debug.Log("z: " + z);
                Debug.Log("direction: " + direction);
                Debug.Log(pawn);
                Debug.Log("Player index: " + pawn.Player.Index);
                Debug.Log("Pawn index: " + pawn.Index);
                grid[x + (int)direction.x, z + (int)direction.z] = pawn.Player.Index * 10 + pawn.Index;
                Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                float eval = Evaluate(grid, 0, pawn, nextPawn);

                if(eval > bestEval)
                {
                    bestEval = eval;
                    dir = direction;
                }
            }
        }

        return length * dir;
    }

    public Vector3 Terrain(Pawn pawn, out Vector3 direction)
    {
        int[,] grid = TerrainManager.Instance.Grid;
        Vector3 coord = Vector3.zero;
        float bestEval = Mathf.NegativeInfinity;
        direction = Vector3.zero;

        for (int z = 0; z < grid.GetLength(1); z++)
        {
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                if (TerrainManager.Instance.CheckLocalTerrainMoveFeasibility(x, z))
                {
                    for (int i = 0; i < m_Directions.Count; i++)
                    {
                        Vector3 dir = m_Directions[i];
                        if (TerrainManager.Instance.IsInsideBounds(x + (int)dir.x, z + (int)dir.z) && grid[x + (int)dir.x, z + (int)dir.z] < 0)
                        {
                            grid[x + (int)dir.x, z + (int)dir.z] = grid[x, z];
                            grid[x, z] = -1;
                            Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                            float eval = Evaluate(grid, 0, pawn, nextPawn);

                            if (eval > bestEval)
                            {
                                bestEval = eval;
                                coord = new Vector3(x, 0, z);
                                direction = dir;
                            }
                        }
                    }
                }
            }
        }

        return coord;
    }

    public Vector3 Destroy(Pawn pawn)
    {
        int[,] grid = TerrainManager.Instance.Grid;
        Vector3 coord = Vector3.zero;
        float bestEval = Mathf.NegativeInfinity;

        for (int z = 0; z < grid.GetLength(1); z++)
        {
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                if (grid[x, z] == 0)
                {
                    grid[x, z] = -1;
                    Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                    float eval = Evaluate(grid, 0, pawn, nextPawn);
                    if (eval > bestEval)
                    {
                        bestEval = eval;
                        coord = new Vector3(x, 0, z);
                    }
                }
            }
        }

        return coord;
    }

    private float Evaluate(int[,] grid, int depth, Pawn maximizingPawn, Pawn pawn)
    {
        // static evaluation
        if (depth == 0 || CheckGameOver(grid))
        {
            return StaticEvaluation(grid, maximizingPawn, pawn);
        }

        if (pawn == maximizingPawn)
        {
            float maxEval = Mathf.NegativeInfinity;
            
            // terrain
            for (int z = 0; z < grid.GetLength(1); z++)
            {
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    if(TerrainManager.Instance.CheckLocalTerrainMoveFeasibility(x, z))
                    {
                        for(int i = 0; i < m_Directions.Count; i++)
                        {
                            Vector3 direction = m_Directions[i];
                            if(TerrainManager.Instance.IsInsideBounds(x + (int)direction.x, z + (int)direction.z) && grid[x + (int)direction.x, z + (int)direction.z] < 0)
                            {
                                grid[x + (int)direction.x, z + (int)direction.z] = grid[x, z];
                                grid[x, z] = -1;
                                Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                                float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                                maxEval = Mathf.Max(maxEval, eval);
                            }
                        }
                    }
                }
            }

            // destroy
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    if(grid[x, y] == 0)
                    {
                        grid[x, y] = -1;
                        Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                        float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                        maxEval = Mathf.Max(maxEval, eval);
                    }
                }
            }

            // move
            for (int i = 0; i < m_Directions.Count; i++)
            {
                Vector3 direction = m_Directions[i];
                int x = pawn.X;
                int z = pawn.Z;

                if(TerrainManager.Instance.IsInsideBounds(x + (int)direction.x, z + (int)direction.z) && grid[x + (int)direction.x, z + (int)direction.z] == 0)
                {
                    grid[x, z] = 0;
                    grid[x + (int)direction.x, z + (int)direction.z] = pawn.Player.Index * 10 + pawn.Index;
                    Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                    float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                    maxEval = Mathf.Max(maxEval, eval);
                }
            }

            // jump
            for (int i = 0; i < m_Directions.Count; i++)
            {
                Vector3 direction = m_Directions[i];
                int x = pawn.X;
                int z = pawn.Z;

                if (TerrainManager.Instance.IsInsideBounds(x + 2 * (int)direction.x, z + 2 * (int)direction.z) && grid[x + 2 * (int)direction.x, z + 2 * (int)direction.z] == 0)
                {
                    grid[x, z] = 0;
                    grid[x + (int)direction.x, z + (int)direction.z] = pawn.Player.Index * 10 + pawn.Index;
                    Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                    float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                    maxEval = Mathf.Max(maxEval, eval);
                }
            }

            return maxEval;
        }
        else
        {
            float minEval = Mathf.Infinity;

            // terrain
            for (int z = 0; z < grid.GetLength(1); z++)
            {
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    if (TerrainManager.Instance.CheckLocalTerrainMoveFeasibility(x, z))
                    {
                        for (int i = 0; i < m_Directions.Count; i++)
                        {
                            Vector3 direction = m_Directions[i];
                            if (TerrainManager.Instance.IsInsideBounds(x + (int)direction.x, z + (int)direction.z) && grid[x + (int)direction.x, z + (int)direction.z] < 0)
                            {
                                grid[x + (int)direction.x, z + (int)direction.z] = grid[x, z];
                                grid[x, z] = -1;
                                Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                                float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                                minEval = Mathf.Min(minEval, eval);
                            }
                        }
                    }
                }
            }

            // destroy
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    if (grid[x, y] == 0)
                    {
                        grid[x, y] = -1;
                        Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                        float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                        minEval = Mathf.Min(minEval, eval);
                    }
                }
            }

            // move
            for (int i = 0; i < m_Directions.Count; i++)
            {
                Vector3 direction = m_Directions[i];
                int x = pawn.X;
                int z = pawn.Z;

                if (TerrainManager.Instance.IsInsideBounds(x + (int)direction.x, z + (int)direction.z) && grid[x + (int)direction.x, z + (int)direction.z] == 0)
                {
                    grid[x, z] = 0;
                    grid[x + (int)direction.x, z + (int)direction.z] = pawn.Player.Index * 10 + pawn.Index;
                    Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                    float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                    minEval = Mathf.Min(minEval, eval);
                }
            }

            // jump
            for (int i = 0; i < m_Directions.Count; i++)
            {
                Vector3 direction = m_Directions[i];
                int x = pawn.X;
                int z = pawn.Z;

                if (TerrainManager.Instance.IsInsideBounds(x + 2 * (int)direction.x, z + 2 * (int)direction.z) && grid[x + 2 * (int)direction.x, z + 2 * (int)direction.z] == 0)
                {
                    grid[x, z] = 0;
                    grid[x + (int)direction.x, z + (int)direction.z] = pawn.Player.Index * 10 + pawn.Index;
                    Pawn nextPawn = GetNextPawnInGrid(grid, pawn);
                    float eval = Evaluate(grid, depth - 1, maximizingPawn, nextPawn);
                    minEval = Mathf.Min(minEval, eval);
                }
            }

            return minEval;
        }
    }

    private Pawn GetNextPawnInGrid(int[,] grid, Pawn pawn)
    {
        int index = pawn.Player.Index * 10 + pawn.Index;

        int nextIndex = 50;
        for (int y = 0; y < grid.GetLength(1); y++)
        {
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                if (grid[x, y] > index && grid[x, y] < nextIndex)
                {
                    nextIndex = grid[x, y];
                }
            }
        }

        if(nextIndex == 50)
        {
            nextIndex = 0;
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    if (grid[x, y] < index && grid[x, y] > nextIndex)
                    {
                        nextIndex = grid[x, y];
                    }
                }
            }
        }

        return PlayerManager.Instance.GetPawnFromIndex(nextIndex);
    }

    private float StaticEvaluation(int[,] grid, Pawn maximizingPawn, Pawn pawn)
    {
        float playerPawns = 0;
        float enemyPawns = 0;

        for(int y = 0; y < grid.GetLength(1); y++)
        {
            for(int x = 0; x < grid.GetLength(0); x++)
            {
                if (grid[x, y] > 0)
                {
                    if(grid[x,y] == maximizingPawn.Player.Index * 10 + 1 || grid[x, y] == maximizingPawn.Player.Index * 10 + 2)
                    {
                        playerPawns += 1f;
                    }
                    else
                    {
                        enemyPawns += .5f;
                    }
                }
            }
        }

        return (playerPawns - enemyPawns);
    }

    private bool CheckGameOver(int[,] grid)
    {
        int player = 0;
        for (int y = 0; y < grid.GetLength(1); y++)
        {
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                if (grid[x, y] > 0 && player == 0)
                    player = grid[x, y];

                if (grid[x, y] > 0 && player > 0 && grid[x, y] != player)
                    return false;
            }
        }
        return true;
    }
}