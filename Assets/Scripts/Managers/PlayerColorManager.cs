﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerColorManager : MonoBehaviour
{
    public List<Color> PlayerColors { get; private set; }

    private void Awake()
    {
        PlayerColors = new List<Color>();
        for (int i = 0; i < TransferManager.Instance.PlayerData.Count; i++)
        {
            PlayerColors.Add(TransferManager.Instance.PlayerData[i].color);
        } 
    }
}