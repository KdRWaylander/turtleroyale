﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{
    public List<PlayerData> PlayerData { get; private set; }

    List<PlayerDataToggle> m_PlayerDataToggleList;
    List<PlayerDataInput> m_PlayerDataInputList;

    public void TooglePlayerDataInput(int index)
    {
        m_PlayerDataToggleList = new List<PlayerDataToggle>();

        m_PlayerDataToggleList.Clear();
        for(int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).GetComponent<PlayerDataToggle>() != null)
            {
                m_PlayerDataToggleList.Add(transform.GetChild(i).GetComponent<PlayerDataToggle>());
            }
        }

        if (index < GetNumbersOfActivePlayerDataInput(m_PlayerDataToggleList))
        {
            for (int i = 0; i < m_PlayerDataToggleList.Count; i++)
            {
                if (i <= index)
                {
                    m_PlayerDataToggleList[i].ToggleOn();
                }
                else if (i > index)
                {
                    m_PlayerDataToggleList[i].ToggleOff();
                }
            }
        }
        else
        {
            for (int i = 0; i < index + 1; i++)
            {
                m_PlayerDataToggleList[i].ToggleOn();
            }
        }

    }

    private int GetNumbersOfActivePlayerDataInput(List<PlayerDataToggle> toggleList)
    {
        int n = 0;
        foreach(PlayerDataToggle playerDataToggle in toggleList)
        {
            if(playerDataToggle.IsActive == true)
            {
                n += 1;
            }
        }
        return n;
    }

    public List<PlayerData> BuildPlayerDataList()
    {
        PlayerData = new List<PlayerData>();

        m_PlayerDataInputList = new List<PlayerDataInput>();
        m_PlayerDataInputList.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<PlayerDataInput>() != null)
            {
                m_PlayerDataInputList.Add(transform.GetChild(i).GetComponent<PlayerDataInput>());
            }
        }

        for (int i = 0; i < m_PlayerDataInputList.Count; i++)
        {
            if (m_PlayerDataInputList[i].IsActive == true)
            {
                PlayerData.Add(m_PlayerDataInputList[i].CollectPlayerData(i));
            }
        }

        return PlayerData;
    }
}