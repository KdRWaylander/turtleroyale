﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance { get; private set; }

    [SerializeField] Player m_PlayerPrefab;
    [SerializeField] Player m_AIPrefab;
    [SerializeField] Pawn m_PawnPrefab;
    [SerializeField] RobotPawn m_RobotPawnPrefab;

    private List<Player> m_Players;
    private Player m_PlayerLackingPawn;
    private bool m_IsPlacingRobotPawns = false;

    public bool IsGameOn { get; private set; }
    public int NumberOfPlayers { get; private set; }
    public int MaxPlayers { get; private set; }
    public int MaxPawnPerPlayer { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_Players = new List<Player>();

        NumberOfPlayers = 0;
        MaxPawnPerPlayer = 2;

        MaxPlayers = TransferManager.Instance.PlayerData.Count;

        for (int i = 0; i < MaxPlayers; i++)
        {
            AddPlayer();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
            TerrainManager.Instance.Grid.DebugLog();

        if (GameManager.Instance.IsGameOver || IsGameOn == true)
            return;

        if (AllPlayersAreReady() && IsGameOn == false)
        {
            StartCoroutine(StartGame());
        }
        else
        {
            PlacePawn();
        }
    }

    private void PlacePawn()
    {
        for (int i = 0; i < m_Players.Count; i++)
        {
            if (m_Players[i].transform.childCount < MaxPawnPerPlayer)
            {
                m_PlayerLackingPawn = m_Players[i];
                TurnManager.Instance.HandleNewPlayerTurn(m_PlayerLackingPawn);
                GameManager.Instance.HandleNewActionDrawn(GameManager.Instance.Action, m_PlayerLackingPawn);
                break;
            }
        }

        if(m_PlayerLackingPawn.AI == false)
        {
            #if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    var hitPos = new Vector2(hit.transform.position.x, hit.transform.position.z);
                    AddPawn(hitPos, m_PlayerLackingPawn);
                }
            }
            #else
            if(Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                    {
                        var hitPos = new Vector2(hit.transform.position.x, hit.transform.position.z);
                        AddPawn(hitPos, m_PlayerLackingPawn);
                    }
                }
            }
            #endif
        }
        else
        {
            if(m_IsPlacingRobotPawns == false)
            {
                StartCoroutine(AddRobotPawn(m_PlayerLackingPawn));
            }
        }
    }

    public Pawn GetPawnFromIndex(int index)
    {
        foreach(Player player in m_Players)
        {
            if(player.Index == Mathf.FloorToInt(index / 10))
            {
                foreach(Pawn pawn in player.Pawns)
                {
                    if(pawn.Index == Mathf.FloorToInt(index % 2))
                    {
                        return pawn;
                    }
                }
            }
        }

        return null;
    }

    private bool AllPlayersAreReady()
    {
        for (int i = 0; i < m_Players.Count; i++)
        {
            if (m_Players[i].transform.childCount < MaxPawnPerPlayer)
            {
                return false;
            }
        }
        return true;
    }

    private IEnumerator StartGame()
    {
        IsGameOn = true;
        yield return new WaitForSeconds(.5f);
        GameManager.Instance.StartGame();
    }

    public void AddPlayer()
    {
        NumberOfPlayers += 1;

        var player = (TransferManager.Instance.PlayerData[NumberOfPlayers - 1].ai) ?
            Instantiate(m_AIPrefab, Vector3.zero, Quaternion.identity, transform):
            Instantiate(m_PlayerPrefab, Vector3.zero, Quaternion.identity, transform);

        player.GetComponent<Player>().SetIndex(NumberOfPlayers);
        player.GetComponent<Player>().SetAIStatus(TransferManager.Instance.PlayerData[NumberOfPlayers - 1].ai);

        TurnManager.Instance.AddPlayer(player.GetComponent<Player>());
        m_Players.Add(player);
    }

    private IEnumerator AddRobotPawn(Player player)
    {
        m_IsPlacingRobotPawns = true;
        yield return new WaitForSeconds(1f);

        Vector2 pos = Vector2.zero;
        bool validPosition = false;
        while (validPosition == false)
        {
            int x = Random.Range(0, 6);
            int z = Random.Range(0, 6);

            pos = new Vector2(x, z);
            validPosition = true;
            foreach (Player plyr in m_Players)
            {
                for (int i = 0; i < plyr.transform.childCount; i++)
                {
                    Vector3 childPosition = plyr.transform.GetChild(i).transform.position;
                    float distance = (childPosition - new Vector3(pos.x, childPosition.y, pos.y)).magnitude;
                    if (distance <= 1)
                    {
                        validPosition = false;
                    }
                }
            }
        }
        AddPawn(pos, player);

        m_IsPlacingRobotPawns = false;
    }

    public void AddPawn(Vector2 pos, Player player)
    {
        foreach (Player plyr in m_Players)
        {
            for (int i = 0; i < plyr.transform.childCount; i++)
            {
                Vector3 childPosition = plyr.transform.GetChild(i).transform.position;
                float distance = (childPosition - new Vector3(pos.x, childPosition.y, pos.y)).magnitude;
                if (distance <= 1)
                {
                    SettingsManager.Instance.PlayWrongSound();
                    return;
                }
            }
        }

        var pawn = player.AI ?
            Instantiate(m_RobotPawnPrefab, new Vector3(pos.x, .5f, pos.y), Quaternion.identity, player.transform):
            Instantiate(m_PawnPrefab, new Vector3(pos.x, .5f, pos.y), Quaternion.identity, player.transform);

        player.AddPawn(pawn);

        if(player.AI == false)
        {
            SettingsManager.Instance.PlayPlaceSound();
        }
    }

    public Player GetPlayer(int index)
    {
        return index >= 0 && index < m_Players.Count ? m_Players[index] : m_Players[0];
    }

    public List<Player> GetPlayers()
    {
        return m_Players;
    }

    public void RemovePawn(Pawn pawn)
    {
        var player = pawn.GetPlayer();

        pawn.EndPawnTurn();
        player.RemovePawn(pawn);
        TerrainManager.Instance.SetCellValue(new Vector2((int)pawn.transform.position.x, (int)pawn.transform.position.z), 0);

        if (player.transform.childCount <= 1)
        {
            RemovePlayer(player);
        }
        Destroy(pawn.gameObject);
    }

    public void RemoveRobotPawn(RobotPawn robotPawn)
    {
        var player = robotPawn.Player;

        robotPawn.EndPawnTurn();
        player.RemoveRobotPawn(robotPawn);
        TerrainManager.Instance.SetCellValue(new Vector2((int)robotPawn.transform.position.x, (int)robotPawn.transform.position.z), 0);

        if (player.transform.childCount <= 1)
        {
            RemovePlayer(player);
        }
        Destroy(robotPawn.gameObject);
    }

    public void RemovePlayer(Player player)
    {
        player.EndPlayerTurn();
        m_Players.Remove(player);
        FindObjectOfType<TurnManager>().RemovePlayer(player);
        FindObjectOfType<TerrainManager>().SetCellValue(new Vector2((int)player.transform.position.x, (int)player.transform.position.z), 0);
        Destroy(player.gameObject);

        if (NumberOfPlayers > 2)
        {
            NumberOfPlayers -= 1;
        }
        else
        {
            GameManager.Instance.GameOver();
        }
    }
}