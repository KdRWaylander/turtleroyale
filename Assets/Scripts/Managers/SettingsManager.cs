﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager Instance { get; private set; }

    public AudioStatus CurrentAudioStatus { get; private set; }
    public bool IsMusicMute { get; private set; }
    public bool IsSFXMute { get; private set; }

    [Header("Snapshots")]
    [SerializeField] private float m_AudioTransitionTime = 0.25f;
    [SerializeField] private AudioMixerSnapshot m_MusicAndSFXSnapshot;
    [SerializeField] private AudioMixerSnapshot m_NoMusicSnapshot;
    [SerializeField] private AudioMixerSnapshot m_NoSFXSnapshot;
    [SerializeField] private AudioMixerSnapshot m_NoMusicNoSFXSnapshot;

    [Header("Place")]
    [SerializeField] private AudioSource m_PlaceAudioSource;
    [SerializeField] private AudioClip m_PlaceSound;

    [Header("Turn")]
    [SerializeField] private AudioSource m_TurnAudioSource;
    [SerializeField] private AudioClip m_TurnSound;

    [Header("Move")]
    [SerializeField] private AudioSource m_MovementAudioSource;
    [SerializeField] private AudioClip m_MovementSound;

    [Header("Teleport")]
    [SerializeField] private AudioSource m_TeleportAudioSource;
    [SerializeField] private AudioClip m_TeleportSound;

    [Header("Destroy")]
    [SerializeField] private AudioSource m_DestroyAudioSource;
    [SerializeField] private AudioClip m_DestroySound;

    [Header("TerrainSelection")]
    [SerializeField] private AudioSource m_TerrainSelectionAudioSource;
    [SerializeField] private AudioClip m_TerrainSelectionSound;

    [Header("TerrainMovement")]
    [SerializeField] private AudioSource m_TerrainMovementAudioSource;
    [SerializeField] private AudioClip m_TerrainMovementSound;

    [Header("Death")]
    [SerializeField] private AudioSource m_DeathAudioSource;
    [SerializeField] private AudioClip m_DeathSound;

    [Header("Victory")]
    [SerializeField] private AudioSource m_VictoryAudioSource;
    [SerializeField] private AudioClip m_VictorySound;

    [Header("Wrong")]
    [SerializeField] private AudioSource m_WrongAudioSource;
    [SerializeField] private AudioClip m_WrongSound;

    private const string AudioStatusKey = "AUDIOSTATUS";
    private const string NoMuteValue = "NoMute";
    private const string MusicMuteValue = "MusicMute";
    private const string SFXMuteValue = "SFXMute";
    private const string AllMuteValue = "AllMute";

    private const string GameSpeedKey = "GAMESPEED";
    private const float SlowValue = .5f;
    private const float NormalValue = 1f;
    private const float FastValue = 2f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        CurrentAudioStatus = GetAudioStatusFromPlayerPrefs();
        SwitchSnapshot(CurrentAudioStatus);

        Time.timeScale = GetGameSpeedFromPlayerPrefs();
    }

    public float GetGameSpeedFromPlayerPrefs()
    {
        if (PlayerPrefs.HasKey(GameSpeedKey) == false)
            PlayerPrefs.SetFloat(GameSpeedKey, NormalValue);

        if(PlayerPrefs.GetFloat(GameSpeedKey) != SlowValue && PlayerPrefs.GetFloat(GameSpeedKey) != NormalValue && PlayerPrefs.GetFloat(GameSpeedKey) != FastValue)
            PlayerPrefs.SetFloat(GameSpeedKey, NormalValue);

        return PlayerPrefs.GetFloat(GameSpeedKey);
    }

    public void SetGameSpeed(GameSpeed gameSpeed)
    {
        switch (gameSpeed)
        {
            case GameSpeed.Slow:
                Time.timeScale = SlowValue;
                PlayerPrefs.SetFloat(GameSpeedKey, SlowValue);
                break;
            case GameSpeed.Normal:
                Time.timeScale = NormalValue;
                PlayerPrefs.SetFloat(GameSpeedKey, NormalValue);
                break;
            case GameSpeed.Fast:
                Time.timeScale = FastValue;
                PlayerPrefs.SetFloat(GameSpeedKey, FastValue);
                break;
        }
    }

    private AudioStatus GetAudioStatusFromPlayerPrefs()
    {
        if (PlayerPrefs.HasKey(AudioStatusKey) == false)
            PlayerPrefs.SetString(AudioStatusKey, NoMuteValue);

        string status = PlayerPrefs.GetString(AudioStatusKey);
        if (status == MusicMuteValue)
        {
            return AudioStatus.MusicMute;
        }
        else if (status == SFXMuteValue)
        {
            return AudioStatus.SFXMute;
        }
        else if (status == AllMuteValue)
        {
            return AudioStatus.AllMute;
        }
        else
        {
            return AudioStatus.NoMute;
        }
    }

    private AudioStatus GetNewAudioStatus()
    {
        if (IsSFXMute == false)
        {
            if (IsMusicMute == false)
                return AudioStatus.NoMute;
            else
                return AudioStatus.MusicMute;
        }
        else
        {
            if (IsMusicMute == false)
                return AudioStatus.SFXMute;
            else
                return AudioStatus.AllMute;
        }
    }

    private void SwitchSnapshot(AudioStatus status)
    {
        switch (status)
        {
            case AudioStatus.NoMute:
                m_MusicAndSFXSnapshot.TransitionTo(m_AudioTransitionTime);
                IsMusicMute = false;
                IsSFXMute = false;
                PlayerPrefs.SetString(AudioStatusKey, NoMuteValue);
                break;
            case AudioStatus.MusicMute:
                m_NoMusicSnapshot.TransitionTo(m_AudioTransitionTime);
                IsMusicMute = true;
                IsSFXMute = false;
                PlayerPrefs.SetString(AudioStatusKey, MusicMuteValue);
                break;
            case AudioStatus.SFXMute:
                m_NoSFXSnapshot.TransitionTo(m_AudioTransitionTime);
                IsMusicMute = false;
                IsSFXMute = true;
                PlayerPrefs.SetString(AudioStatusKey, SFXMuteValue);
                break;
            case AudioStatus.AllMute:
                m_NoMusicNoSFXSnapshot.TransitionTo(m_AudioTransitionTime);
                IsMusicMute = true;
                IsSFXMute = true;
                PlayerPrefs.SetString(AudioStatusKey, AllMuteValue);
                break;
        }
    }

    public bool ToggleMusic()
    {
        IsMusicMute = !IsMusicMute;

        CurrentAudioStatus = GetNewAudioStatus();
        SwitchSnapshot(CurrentAudioStatus);

        return IsMusicMute;
    }

    public bool ToggleSound()
    {
        IsSFXMute = !IsSFXMute;

        CurrentAudioStatus = GetNewAudioStatus();
        SwitchSnapshot(CurrentAudioStatus);

        return IsSFXMute;
    }

    public void PlayPlaceSound()
    {
        m_PlaceAudioSource.PlayOneShot(m_PlaceSound);
    }

    public void PlayTurnSound()
    {
        m_TurnAudioSource.PlayOneShot(m_TurnSound);
    }

    public void PlayMovementSound()
    {
        m_MovementAudioSource.PlayOneShot(m_MovementSound);
    }

    public void PlayTeleportSound()
    {
        m_TeleportAudioSource.PlayOneShot(m_TeleportSound);
    }

    public void PlayDestroySound()
    {
        m_DestroyAudioSource.PlayOneShot(m_DestroySound);
    }

    public void PlayTerrainSelectionSound()
    {
        m_TerrainSelectionAudioSource.PlayOneShot(m_TerrainSelectionSound);
    }

    public void PlayTerrainMovementSound()
    {
        m_TerrainMovementAudioSource.PlayOneShot(m_TerrainMovementSound);
    }

    public void PlayPawnDeathSound()
    {
        m_DeathAudioSource.PlayOneShot(m_DeathSound);
    }

    public void PlayVictorySound()
    {
        StartCoroutine(VictorySounnd());
    }

    private IEnumerator VictorySounnd()
    {
        yield return new WaitForSeconds(.5f);
        m_VictoryAudioSource.PlayOneShot(m_VictorySound);
    }

    public void PlayWrongSound()
    {
        m_WrongAudioSource.PlayOneShot(m_WrongSound);
    }
}

[Serializable]
public enum GameSpeed
{
    Slow,
    Normal,
    Fast
}

[Serializable]
public enum AudioStatus
{
    NoMute,
    MusicMute,
    SFXMute,
    AllMute
}