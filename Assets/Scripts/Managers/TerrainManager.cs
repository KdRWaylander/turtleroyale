﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TerrainManager : MonoBehaviour
{
    public static TerrainManager Instance { get; private set; }
    public int[,] Grid { get { return m_Grid; } }

    private int[,] m_Grid;
    private List<GameObject> m_AlreadyMovedTerrainChunks;

    private void Awake()
    {
        Instance = this;

        m_Grid = new int[6, 6];
        m_AlreadyMovedTerrainChunks = new List<GameObject>();
    }

    public TerrainChunk GetTerrainChunk(int x, int z)
    {
        TerrainChunk[] terrainChunks = FindObjectsOfType<TerrainChunk>();
        foreach (TerrainChunk terrainChunk in terrainChunks)
        {
            if (terrainChunk.transform.position.x == x && terrainChunk.transform.position.z == z)
            {
                return terrainChunk;
            }
        }
        return null;
    }

    public void DestroyTerrainChunk(Transform terrainChunkTransform)
    {
        SetCellValue(new Vector2(terrainChunkTransform.position.x, terrainChunkTransform.position.z), -1);
        Destroy(terrainChunkTransform.gameObject);
    }

    public void DestroyTerrainChunk(Vector2 position)
    {
        TerrainChunk[] terrainChunks = FindObjectsOfType<TerrainChunk>();
        foreach(TerrainChunk terrainChunk in terrainChunks)
        {
            if(terrainChunk.transform.position.x == position.x && terrainChunk.transform.position.z == position.y)
            {
                SetCellValue(position, -1);
                Destroy(terrainChunk.gameObject);
                break;
            }
        }
    }

    public void DestroyTerrainChunk(Vector3 position)
    {
        DestroyTerrainChunk(new Vector2(position.x, position.z));
    }

    public void MoveTerrainChunk(GameObject chunk, Vector3 direction)
    {
        SetCellValue(new Vector2(chunk.transform.position.x, chunk.transform.position.z), -1);
        SetCellValue(new Vector2((chunk.transform.position + direction).x, (chunk.transform.position + direction).z), 0);

        chunk.transform.position += direction;

        m_AlreadyMovedTerrainChunks.Add(chunk);
    }

    public int GetCellValue(Vector2 cellCoordinates)
    {
        return m_Grid[(int)cellCoordinates.x, (int)cellCoordinates.y];
    }

    public void SetCellValue(Vector2 cellCoordinates, int value)
    {
        m_Grid[(int)cellCoordinates.x, (int)cellCoordinates.y] = value;
    }

    public bool CheckActionFeasibility(TurnAction action)
    {
        if (action == TurnAction.Destroy)
        {
            for (int y = m_Grid.GetLength(1) - 1; y > -1; y--)
            {
                for (int x = 0; x < m_Grid.GetLength(0); x++)
                {
                    if (m_Grid[x, y] == 0)
                        return true;
                }
            }
        }
        else if (action == TurnAction.Terrain)
        {
            for (int y = m_Grid.GetLength(1) - 1; y > -1; y--)
            {
                for (int x = 0; x < m_Grid.GetLength(0); x++)
                {
                    if (m_Grid[x, y] == -1)
                        return true;
                }
            }
        }
        else
        {
            return true;
        }

        return false;
    }

    public bool CheckActionFeasibility(Vector3 playerPosition, TurnAction action)
    {
        if (action == TurnAction.Move || action == TurnAction.Jump)
        {
            float multiplier = (action == TurnAction.Jump) ? 2 : 1;
            List<Vector3> potentialPositions = new List<Vector3>();
            potentialPositions.Add(playerPosition + multiplier * Vector3.forward);
            potentialPositions.Add(playerPosition - multiplier * Vector3.forward);
            potentialPositions.Add(playerPosition + multiplier * Vector3.right);
            potentialPositions.Add(playerPosition - multiplier * Vector3.right);

            foreach (Vector3 newPos in potentialPositions)
            {
                int x = (int)newPos.x;
                int z = (int)newPos.z;
                
                if(IsInsideBounds(x, z))
                {
                    if (m_Grid[(int)newPos.x, (int)newPos.z] == 0)
                        return true;
                }
            }
        }
        else
        {
            var feasibility = CheckActionFeasibility(action);
            return feasibility;
        }

        return false;
    }

    public bool CheckLocalTerrainMoveFeasibility(int x, int y)
    {
        return CheckLocalTerrainMoveFeasibility(new Vector3(x, 0, y));
    }

    public bool CheckLocalTerrainMoveFeasibility(Vector3 localPosition)
    {
        List<Vector3> potentialPositions = new List<Vector3>();
        potentialPositions.Add(localPosition + Vector3.forward);
        potentialPositions.Add(localPosition - Vector3.forward);
        potentialPositions.Add(localPosition + Vector3.right);
        potentialPositions.Add(localPosition - Vector3.right);

        foreach (Vector3 newPos in potentialPositions)
        {
            int x = (int)newPos.x;
            int z = (int)newPos.z;

            if (IsInsideBounds(x, z))
            {
                if (m_Grid[(int)newPos.x, (int)newPos.z] == -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool HasAlreadyMoved(GameObject chunk)
    {
        foreach (GameObject go in m_AlreadyMovedTerrainChunks)
        {
            if (chunk.name == go.name)
                return true;
        }

        return false;
    }

    public void ClearAlreadyMovedTerrainChunksList()
    {
        m_AlreadyMovedTerrainChunks.Clear();
    }

    public bool CheckPositionValidity(Vector3 playerPosition,  Vector3 direction, int movementLength)
    {
        if (direction.magnitude != movementLength)
            return false;

        int x = (int)(playerPosition + direction).x;
        int z = (int)(playerPosition + direction).z;

        if (IsInsideBounds(x, z) == false)
        {
            return false;
        }
        else
        {
            if (CheckCellOccupation(new Vector2(x, z))) // returns true if cell is occupied (grid value at x,y = 1)
            {
                return false;
            }
        }

        return true;
    }

    public bool IsInsideBounds(int x, int z)
    {
        return !(x < 0 || x >= m_Grid.GetLength(0) || z < 0 || z >= m_Grid.GetLength(1));
    }

    public bool CheckCellOccupation(Vector2 cellCoordinates)
    {
        int x = (int)cellCoordinates.x;
        int z = (int)cellCoordinates.y;

        if (m_Grid[x, z] > 0 || m_Grid[x, z] == -1) // >0: case occupée, -1: case inexistante
            return true;
        return false;
    }

    public bool CheckMovementValidity(Vector2 cellCoordinates)
    {
        int x = (int)cellCoordinates.x;
        int z = (int)cellCoordinates.y;

        return CheckMovementValidity(x, z);
    }

    private bool CheckMovementValidity(int x, int z)
    {
        if (IsInsideBounds(x, z) == false)
            return false;

        if (m_Grid[x, z] == -1)
            return true;

        return false;
    }
}