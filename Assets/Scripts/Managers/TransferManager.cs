﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TransferManager : MonoBehaviour
{
    public static TransferManager Instance { get; private set; }

    public List<PlayerData> PlayerData { get; private set; }

    private PlayerDataManager m_PlayerDataCollection;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PlayerData = new List<PlayerData>();
    }

    public void CollectPlayerData()
    {
        m_PlayerDataCollection = FindObjectOfType<PlayerDataManager>();
        m_PlayerDataCollection.BuildPlayerDataList();

        PlayerData.Clear();
        for (int i = 0; i < m_PlayerDataCollection.PlayerData.Count; i++)
        {
            PlayerData.Add(m_PlayerDataCollection.PlayerData[i]);
        }
    }
}

[System.Serializable]
public struct PlayerData
{
    public string name;
    public int index;
    public Color color;
    public bool ai;

    public PlayerData(string name, int index, Color color, bool ai)
    {
        this.name = name;
        this.index = index;
        this.color = color;
        this.ai = ai;
    }
}