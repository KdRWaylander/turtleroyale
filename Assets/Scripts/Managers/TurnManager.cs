﻿ using System;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public static TurnManager Instance { get; private set; }
    public int Turns { get; private set; }
    public Player ActivePlayer { get; private set; }

    public List<TurnPlayer> players;

    public event Action<Player> NewPlayerTurn;
    public event Action NewTurn;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Turns = 0;
    }

    private void Update()
    {
        if (!GameManager.Instance.IsGameOn || GameManager.Instance.IsGameOver)
            return;

        UpdateTurns();
    }

    public void AddPlayerToList(Player player)
    {
        TurnPlayer tp = new TurnPlayer();
        tp.player = player.gameObject;
        players.Add(tp);
    }

    public void ResetTurns()
    {
        for (int i = 0; i < players.Count; i++)
        {
            players[i].isTurn = false;
            players[i].hasPlayed = false;
        }
        players[0].isTurn = true;
        HandleNewTurn();
        HandleNewPlayerTurn(players[0].player.GetComponent<Player>());
        GameManager.Instance.DrawNewAction();
    }

    private void UpdateTurns()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (!players[i].hasPlayed)
            {
                players[i].isTurn = true;
                var player = players[i].player.GetComponent<Player>();
                HandleNewPlayerTurn(player);
                GameManager.Instance.HandleNewActionDrawn(GameManager.Instance.Action, player);
                break;
            }
            else if (i == players.Count - 1 && players[i].hasPlayed == true)
            {
                ResetTurns();
            }
        }
    }

    public void AddPlayer(Player player)
    {
        TurnPlayer turnPlayer = new TurnPlayer();
        turnPlayer.player = player.gameObject;
        players.Add(turnPlayer);
    }

    public void RemovePlayer(Player player)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].player.name == player.name)
            {
                players.RemoveAt(i);
                return;
            }
        }
    }

    public void HandleNewPlayerTurn(Player player)
    {
        if (NewPlayerTurn != null)
        {
            NewPlayerTurn(player);
        }
    }

    public void HandleNewTurn()
    {
        Turns += 1;
        if (NewTurn != null)
        {
            NewTurn();
        }
    }
}

[Serializable]
public class TurnPlayer
{
    public GameObject player;
    public bool isTurn;
    public bool hasPlayed;
}