﻿using System.Collections;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    public int Index { get; private set; }
    public int NumberOfPlayedActions { get; set; }
    public bool IsTurn { get; set; }
    public bool HasPlayed { get; set; }
    public Player Player { get { return m_Player; } }
    public int X { get { return (int)transform.position.x; } }
    public int Z { get { return (int)transform.position.z; } }

    private Player m_Player;
    private Animator m_Animator;
    private PawnColor m_PawnColor;
    private PawnMovement m_PawnMovement;
    private PawnActionDestroy m_PawnActionDestroy;
    private PawnActionTerrain m_PawnActionTerrain;

    private GameObject m_Belt;

    private void Awake()
    {
        m_Player = GetComponentInParent<Player>();
        m_Animator = GetComponentInChildren<Animator>();
        m_PawnColor = GetComponent<PawnColor>();
        m_PawnMovement = GetComponent<PawnMovement>();
        m_PawnActionDestroy = GetComponent<PawnActionDestroy>();
        m_PawnActionTerrain = GetComponent<PawnActionTerrain>();
        m_Belt = GetComponentInChildren<Belt>().gameObject;
    }

    private void Start()
    {
        Index = transform.parent.childCount;
        name = "Pawn " + Index;

        NumberOfPlayedActions = 0;
        HasPlayed = false;

        m_Belt.GetComponent<MeshRenderer>().material.color = FindObjectOfType<PlayerColorManager>().PlayerColors[GetComponentInParent<Player>().Index - 1];
        ToggleBelt(false);

        TerrainManager.Instance.SetCellValue(new Vector2(transform.position.x, transform.position.z), Player.Index * 10 + Index);
    }

    private void Update()
    {
        if (IsTurn && NumberOfPlayedActions < GameManager.Instance.MaxNumberOfPlayedActions)
        {
            // Check if player can do its action. If not he loses. Exception for terrain movement, the action is only skipped
            if (TerrainManager.Instance.CheckActionFeasibility(transform.position, GameManager.Instance.Action) == false)
            {
                if (GameManager.Instance.Action == TurnAction.Move || GameManager.Instance.Action == TurnAction.Jump)
                {
                    StartCoroutine(RemovePawn());
                }
                else
                {
                    EndPawnTurn();
                }
                return;
            }

            // If action is possible
            ToggleBelt(true);
            Action();
        }

        if (HasPlayed)
        {
            EndPawnTurn();
        }
    }

    private IEnumerator RemovePawn()
    {
        ToggleBelt(true);
        m_Animator.SetTrigger("Death");
        yield return new WaitForSeconds(1.3f);
        SettingsManager.Instance.PlayPawnDeathSound();
        PlayerManager.Instance.RemovePawn(this);
    }

    public void Action()
    {
        if (GameManager.Instance.Action == TurnAction.Move)
        {
            m_PawnMovement.Move();
        }
        else if (GameManager.Instance.Action == TurnAction.Jump)
        {
            m_PawnMovement.Jump();
        }
        else if (GameManager.Instance.Action == TurnAction.Destroy)
        {
            m_PawnActionDestroy.Destroy();
        }
        else if (GameManager.Instance.Action == TurnAction.Terrain)
        {
            m_PawnActionTerrain.Move();
        }
    }

    public Player GetPlayer()
    {
        return m_Player;
    }

    public void ToggleBelt(bool toggle)
    {
        m_Belt.SetActive(toggle);
    }

    public void EndPawnTurn()
    {
        ToggleBelt(false);

        NumberOfPlayedActions = 0;
        IsTurn = false;
        HasPlayed = true;
    }
}