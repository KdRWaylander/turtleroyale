﻿using System.Collections;
using UnityEngine;

public class PawnActionDestroy : MonoBehaviour
{
    private Pawn m_Pawn;
    private TerrainManager m_TerrainManager;

    private void Awake()
    {
        m_Pawn = GetComponent<Pawn>();
        m_TerrainManager = FindObjectOfType<TerrainManager>();
    }

    public void Destroy()
    {
        StartCoroutine(Destroyment());
    }

    private IEnumerator Destroyment()
    {
        // HERE
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                && m_TerrainManager.GetCellValue(new Vector2(hit.transform.position.x, hit.transform.position.z)) == 0)
            {
                m_Pawn.NumberOfPlayedActions += 1;
                m_Pawn.ToggleBelt(false);

                SettingsManager.Instance.PlayDestroySound();
                m_TerrainManager.DestroyTerrainChunk(hit.transform);

                yield return new WaitForSeconds(.5f);

                m_Pawn.IsTurn = false;
                m_Pawn.HasPlayed = true;
            }
        }
        #else
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                    && m_TerrainManager.GetCellValue(new Vector2(hit.transform.position.x, hit.transform.position.z)) == 0)
                {
                    m_Pawn.NumberOfPlayedActions += 1;
                    m_Pawn.ToggleBelt(false);

                    SettingsManager.Instance.PlayDestroySound();
                    m_TerrainManager.DestroyTerrainChunk(hit.transform);

                    yield return new WaitForSeconds(.5f);

                    m_Pawn.IsTurn = false;
                    m_Pawn.HasPlayed = true;
                }
            }
        }
        #endif
    }
}