﻿using System.Collections;
using UnityEngine;

public class PawnActionTerrain : MonoBehaviour
{
    [SerializeField]
    private LayerMask m_LayerMask;

    private TerrainChunk m_SelectedTerrainChunk;

    private Pawn m_Pawn;
    private PawnMovement m_PawnMovement;
    private TerrainManager m_TerrainManager;

    private void Awake()
    {
        m_Pawn = GetComponent<Pawn>();
        m_PawnMovement = GetComponent<PawnMovement>();
        m_TerrainManager = FindObjectOfType<TerrainManager>();
    }

    private void Start()
    {
        m_SelectedTerrainChunk = null;
    }

    public void Move()
    {
        StartCoroutine(Movement());
    }

    private IEnumerator Movement()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, m_LayerMask))
            {
                if (m_TerrainManager.HasAlreadyMoved(hit.transform.gameObject) == false && m_TerrainManager.CheckLocalTerrainMoveFeasibility(hit.transform.position))
                {
                    // If a terrain chunk is already selected, unhighlight it
                    if (m_SelectedTerrainChunk != null)
                    {
                        m_SelectedTerrainChunk.Highlight(false, Color.white);
                    }

                    // Save the knewly selected chunk and highlight it
                    SettingsManager.Instance.PlayTerrainSelectionSound();
                    m_SelectedTerrainChunk = hit.transform.gameObject.GetComponent<TerrainChunk>();
                    m_SelectedTerrainChunk.Highlight(true, m_Pawn.GetComponent<PawnColor>().BaseColor);
                }
            }
        }
        #else
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, m_LayerMask))
                {
                    if (m_TerrainManager.HasAlreadyMoved(hit.transform.gameObject) == false && m_TerrainManager.CheckLocalTerrainMoveFeasibility(hit.transform.position))
                    {
                        // If a terrain chunk is already selected, unhighlight it
                        if (m_SelectedTerrainChunk != null)
                        {
                            m_SelectedTerrainChunk.Highlight(false, Color.white);
                        }

                        // Save the knewly selected chunk and highlight it
                        SettingsManager.Instance.PlayTerrainSelectionSound();
                        m_SelectedTerrainChunk = hit.transform.gameObject.GetComponent<TerrainChunk>();
                        m_SelectedTerrainChunk.Highlight(true, m_Pawn.GetComponent<PawnColor>().BaseColor);
                    }
                }
            }
        }
        #endif

        if (m_SelectedTerrainChunk != null)
        {
            var direction = m_PawnMovement.GetPlayerInputTerrainMovementDirection(m_SelectedTerrainChunk.gameObject);
            var newPos = new Vector3(m_SelectedTerrainChunk.transform.position.x + direction.x, 0, m_SelectedTerrainChunk.transform.position.z + direction.z);

            if (m_TerrainManager.IsInsideBounds((int)newPos.x, (int)newPos.z) == true)
            {
                if (direction != Vector3.zero && direction.magnitude == 1 && m_TerrainManager.CheckMovementValidity(new Vector2(newPos.x, newPos.z)))
                {
                    m_Pawn.NumberOfPlayedActions += 1;
                    m_Pawn.ToggleBelt(false);

                    yield return new WaitForSeconds(.5f);

                    SettingsManager.Instance.PlayTerrainMovementSound();
                    if (m_TerrainManager.GetCellValue(new Vector2(m_SelectedTerrainChunk.transform.position.x, m_SelectedTerrainChunk.transform.position.z)) > 0)
                    {
                        Vector3 chunkPosition = m_SelectedTerrainChunk.transform.position;
                        TerrainManager.Instance.MoveTerrainChunk(m_SelectedTerrainChunk.gameObject, direction);

                        bool hasMove = false;
                        PawnMovement[] pawns = FindObjectsOfType<PawnMovement>();
                        for (int i = 0; i < pawns.Length; i++)
                        {
                            if (pawns[i].transform.position.x == chunkPosition.x && pawns[i].transform.position.z == chunkPosition.z)
                            {
                                pawns[i].Move(direction);
                                hasMove = true;
                                break;
                            }
                        }
                        if(hasMove == false)
                        {
                            RobotPawnMovement[] robotPawns = FindObjectsOfType<RobotPawnMovement>();
                            for (int i = 0; i < robotPawns.Length; i++)
                            {
                                if (robotPawns[i].transform.position.x == chunkPosition.x && robotPawns[i].transform.position.z == chunkPosition.z)
                                {
                                    robotPawns[i].Move(direction);
                                    hasMove = true;
                                    break;
                                }
                            }

                        }
                    }
                    else
                    {
                        m_TerrainManager.MoveTerrainChunk(m_SelectedTerrainChunk.gameObject, direction);
                    }

                    m_SelectedTerrainChunk.Highlight(false, Color.white);
                    m_SelectedTerrainChunk = null;

                    m_Pawn.IsTurn = false;
                    m_Pawn.HasPlayed = true;
                }
            }
        }
    }
}