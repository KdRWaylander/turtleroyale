﻿using UnityEngine;

public class PawnColor : MonoBehaviour
{
    public Color BaseColor { get; private set; }
    public Color InactiveColor { get; private set; }

    private TurtleColorizer m_MeshRenderer;

    private void Awake()
    {
        m_MeshRenderer = GetComponentInChildren<TurtleColorizer>();
    }

    private void Start()
    {
        BaseColor = TransferManager.Instance.PlayerData[GetComponentInParent<Player>().Index - 1].color;
        InactiveColor = Color.Lerp(BaseColor, Color.black, .5f);

        SetActiveColor();
    }

    public void SetActiveColor()
    {
        m_MeshRenderer.ChangeColor(BaseColor);
    }
}