﻿using System.Collections;
using UnityEngine;

public class PawnMovement : MonoBehaviour
{
    private Pawn m_Pawn;
    private TurtleAnimator m_TurtleAnimator;

    private void Awake()
    {
        m_Pawn = GetComponent<Pawn>();
        m_TurtleAnimator = GetComponentInChildren<TurtleAnimator>();
    }

    public void Move(Vector3 direction)
    {
        transform.position += direction;
        TerrainManager.Instance.SetCellValue(new Vector2(transform.position.x, transform.position.z), m_Pawn.Player.Index * 10 + m_Pawn.Index);
    }

    public void Move()
    {
        StartCoroutine(Movement(1));
    }

    public void Jump()
    {
        StartCoroutine(Movement(2));
    }

    public Vector3 GetPlayerInputMovementDirection()
    {
        // HERE
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                var dir = (hit.transform.position + .5f * Vector3.up - transform.position);
                return dir;
            }
        }
        return Vector3.zero;
        #else
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    var dir = (hit.transform.position + .5f * Vector3.up - transform.position);
                    return dir;
                }
            }
        }
        return Vector3.zero;
        #endif
    }

    private IEnumerator Movement(int length)
    {
        Vector3 direction = GetPlayerInputMovementDirection();
        if (TerrainManager.Instance.CheckPositionValidity(transform.position, direction, length))
        {
            m_Pawn.NumberOfPlayedActions += 1;
            m_Pawn.ToggleBelt(false);
            transform.forward = direction.normalized;

            if(length == 1)
            {
                Vector3 destination = transform.position + direction;

                SettingsManager.Instance.PlayMovementSound();
                m_TurtleAnimator.TriggerMoveAnimation();

                float duration = 1.5f;
                float elapsedTime = 0f;
                float speed = .65f;

                while (elapsedTime < duration)
                {
                    transform.position += direction * Time.deltaTime * speed;
                    yield return null;
                    elapsedTime += Time.deltaTime;
                }

                transform.position = destination;
                m_TurtleAnimator.TriggerIdleAnimation();
            }
            else
            {
                m_TurtleAnimator.TriggerRetreatAnimation();
                yield return new WaitForSeconds(1.2f);
                SettingsManager.Instance.PlayTeleportSound();
                transform.position += direction;
                m_TurtleAnimator.TriggerRetreatAnimation();
                yield return new WaitForSeconds(1.2f);
            }

            TerrainManager.Instance.SetCellValue(new Vector2(transform.position.x, transform.position.z), m_Pawn.Player.Index * 10 + m_Pawn.Index);
            TerrainManager.Instance.SetCellValue(new Vector2((transform.position - direction).x, (transform.position - direction).z), 0);

            m_Pawn.IsTurn = false;
            m_Pawn.HasPlayed = true;
        }
    }

    public Vector3 GetPlayerInputTerrainMovementDirection(GameObject terrainChunk)
    {
        // HERE
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            var worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var position = new Vector3(Mathf.Round(worldPosition.x), 0, Mathf.Round(worldPosition.z));
            return position - terrainChunk.transform.position;
        }
        return Vector3.zero;
        #else
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                var worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var position = new Vector3(Mathf.Round(worldPosition.x), 0, Mathf.Round(worldPosition.z));
                return position - terrainChunk.transform.position;
            }
        }
        return Vector3.zero;
        #endif
    }
}