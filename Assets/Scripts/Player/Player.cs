﻿using UnityEngine;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    [SerializeField] private int m_Index;

    private TurnPlayer m_TurnPlayer;
    private bool m_IsTurn = false;
    private bool m_AI = false;

    public int Index { get { return m_Index; } }
    public bool HasPlayed { get; set; }
    public List<Pawn> Pawns { get; private set; }
    public bool AI { get { return m_AI; } }

    private void Start()
    {
        name = TransferManager.Instance.PlayerData[Index - 1].name;

        foreach (TurnPlayer tp in TurnManager.Instance.players)
        {
            if (tp.player.name == gameObject.name)
                m_TurnPlayer = tp;
        }

        HasPlayed = false;
        Pawns = new List<Pawn>();
    }

    private void Update()
    {
        if (GameManager.Instance.IsGameOver)
            return;

        if (GameManager.Instance.IsGameOn)
        {
            m_IsTurn = m_TurnPlayer.isTurn;
            if(m_IsTurn)
            {
                HasPlayed = HaveAllPawnsPlayed();
                if (HasPlayed == false)
                {
                    GetActivePawn().IsTurn = true;
                }
                else
                {
                    EndPlayerTurn();
                }
            }
        }
    }

    public void SetIndex(int index)
    {
        m_Index = index;
    }

    public void SetAIStatus(bool status)
    {
        m_AI = status;
    }

    private Pawn GetActivePawn()
    {
        for (int i = 0; i < Pawns.Count; i++)
        {
            if (Pawns[i].HasPlayed == false)
            {
                return Pawns[i];
            }
        }
        return Pawns[0];
    }

    private bool HaveAllPawnsPlayed()
    {
        for (int i = 0; i < Pawns.Count; i++)
        {
            if (Pawns[i].HasPlayed == false)
            {
                return false;
            }
        }
        return true;
    }

    public void SetPawnsToActiveColor()
    {
        foreach (Pawn pawn in Pawns)
        {
            pawn.GetComponent<PawnColor>().SetActiveColor();
        }
    }

    public void RemovePawn(Pawn pawn)
    {
        Pawns.Remove(pawn);
    }

    public void RemoveRobotPawn(RobotPawn robotPawn)
    {
        Pawns.Remove(robotPawn);
    }

    public void AddPawn(Pawn pawn)
    {
        Pawns.Add(pawn);
    }

    public void AddRobotPawn(RobotPawn robotPawn)
    {
        Pawns.Add(robotPawn);
    }

    public void EndPlayerTurn()
    {
        for (int i = 0; i < Pawns.Count; i++)
        {
            Pawns[i].HasPlayed = false;
            Pawns[i].NumberOfPlayedActions = 0;
        }

        HasPlayed = false;
        m_TurnPlayer.isTurn = false;
        m_TurnPlayer.hasPlayed = true;
    }
}