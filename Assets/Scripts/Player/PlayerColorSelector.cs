﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerColorSelector : MonoBehaviour
{
    public Color ChosenColor { get; private set; }
    public int CurrentColorIndex { get; set; }

    [SerializeField] private Image m_Image;
    [SerializeField] private int m_DefaultColorIndex;

    private void Start()
    {
        CurrentColorIndex = m_DefaultColorIndex;
    }

    public void ApplyDefaultColor()
    {
        ChosenColor = ColorSelectionManager.Instance.ColorList[m_DefaultColorIndex];
        ColorSelectionManager.Instance.UsedColorsList.Add(ChosenColor);
        m_Image.color = ChosenColor;
    }

    public void BrowseUp()
    {
        Color previousColor = ChosenColor;
        ChosenColor = ColorSelectionManager.Instance.GetNextColor(this);
        m_Image.color = ChosenColor;

        ColorSelectionManager.Instance.UsedColorsList.Remove(previousColor);
        ColorSelectionManager.Instance.UsedColorsList.Add(ChosenColor);
    }

    public void BrowseDown()
    {
        Color previousColor = ChosenColor;
        ChosenColor = ColorSelectionManager.Instance.GetPreviousColor(this);
        m_Image.color = ChosenColor;

        ColorSelectionManager.Instance.UsedColorsList.Remove(previousColor);
        ColorSelectionManager.Instance.UsedColorsList.Add(ChosenColor);
    }

    public void ReleaseColor()
    {
        ColorSelectionManager.Instance.UsedColorsList.Remove(ChosenColor);
        ChosenColor = Color.white;
        CurrentColorIndex = m_DefaultColorIndex;
        m_Image.color = ChosenColor;
    }

    public void GetNewColor()
    {
        if (ColorSelectionManager.Instance.UsedColorsList.Contains(ColorSelectionManager.Instance.ColorList[m_DefaultColorIndex]) == false)
        {
            ApplyDefaultColor();
        }
        else
        {
            ChosenColor = ColorSelectionManager.Instance.GetNextColor(this);
            ColorSelectionManager.Instance.UsedColorsList.Add(ChosenColor);
            m_Image.color = ChosenColor;
        }
    }
}