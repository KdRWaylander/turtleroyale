﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDataInput : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_PlaceHolderText;
    [SerializeField] private TextMeshProUGUI m_InputText;
    [SerializeField] private Image m_PlayerColor;
    [SerializeField] private UIAI m_AI;

    public bool IsActive { get { return transform.GetChild(1).gameObject.activeSelf; } }

    public PlayerData CollectPlayerData(int index)
    {
        return new PlayerData(m_InputText.text.Length > 1 ? m_InputText.text : m_PlaceHolderText.text, index, m_PlayerColor.color, m_AI.AI);
    }
}