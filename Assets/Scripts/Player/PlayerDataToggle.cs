﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerDataToggle : MonoBehaviour
{
    [SerializeField] private TMP_InputField m_InputField;
    [SerializeField] private PlayerColorSelector m_ColorSelector;
    [SerializeField] private Button m_IAButton;
    [SerializeField] private bool m_IsTogglable;

    public bool IsActive { get { return transform.GetChild(1).gameObject.activeSelf; } }
    public bool IsTogglable { get { return m_IsTogglable; } }

    public void ToggleOff()
    {
        if (IsActive == false)
            return;

        m_ColorSelector.ReleaseColor();
        m_InputField.gameObject.SetActive(false);
        m_IAButton.gameObject.SetActive(false);
        m_ColorSelector.gameObject.SetActive(false);
    }

    public void ToggleOn()
    {
        if (IsActive == true)
            return;

        m_ColorSelector.GetNewColor();
        m_InputField.gameObject.SetActive(true);
        m_IAButton.gameObject.SetActive(true);
        m_ColorSelector.gameObject.SetActive(true);
    }
}