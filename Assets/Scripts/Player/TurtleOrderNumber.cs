﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TurtleOrderNumber : MonoBehaviour
{
    private Player m_Player;
    private TextMeshProUGUI m_Text;

    private void Awake()
    {
        m_Player = GetComponentInParent<Player>();
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        m_Text.text = m_Player.transform.childCount.ToString();

        Color col = m_Player.AI ?
            GetComponentInParent<RobotPawnColor>().BaseColor:
            GetComponentInParent<PawnColor>().BaseColor;

        m_Text.faceColor = new Color(Mathf.Sqrt(col.r), Mathf.Sqrt(col.g), Mathf.Sqrt(col.b));
    }
}