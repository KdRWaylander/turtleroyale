﻿using System.Collections.Generic;
using UnityEngine;

public static class RobotBrain
{
    private static List<Vector3> m_Directions = new List<Vector3>() { Vector3.right, Vector3.forward, Vector3.left, Vector3.back};

    public static Vector3 GetDirection(RobotPawn pawn, int length)
    {
        // Copy current grid to manipulate it
        int[,] newGrid = new int[TerrainManager.Instance.Grid.GetLength(0), TerrainManager.Instance.Grid.GetLength(1)];
        for(int z = 0; z < TerrainManager.Instance.Grid.GetLength(1); z++)
        {
            for(int x = 0; x < TerrainManager.Instance.Grid.GetLength(0); x++)
            {
                newGrid[x, z] = TerrainManager.Instance.Grid[x, z];
            }
        }
        List<Vector3> potentialDirections = new List<Vector3>();
        List<int> potentialCounts = new List<int>();
        int bestCount = 0;

        // Get pawn's position on the grid
        int xPawn = (int)pawn.transform.position.x;
        int zPawn = (int)pawn.transform.position.z;

        foreach(Vector3 direction in m_Directions)
        {
            // if move along direction is possible
            int newXPawn = (int)(xPawn + length * direction.x);
            int newZPawn = (int)(zPawn + length * direction.z);
            if (TerrainManager.Instance.IsInsideBounds(newXPawn, newZPawn) && newGrid[newXPawn, newZPawn] == 0)
            {
                // clear cell
                newGrid[xPawn, zPawn] = 0;
                // change new cell value
                newGrid[newXPawn, newZPawn] = pawn.Player.Index * 10 + pawn.Index;
                // check for all 8 movements
                int count = 0;
                foreach(Vector3 dir in m_Directions)
                {
                    // count all possible moves
                    // length = 1
                    if (TerrainManager.Instance.IsInsideBounds((int)(newXPawn + dir.x), (int)(newZPawn + dir.z)) && newGrid[(int)(newXPawn + dir.x), (int)(newZPawn + dir.z)] == 0)
                        count += 1;
                    // length = 2
                    if (TerrainManager.Instance.IsInsideBounds((int)(newXPawn + 2 * dir.x), (int)(newZPawn + 2 * dir.z)) && newGrid[(int)(newXPawn + 2 * dir.x), (int)(newZPawn + 2 * dir.z)] == 0)
                        count += 1;
                }
                potentialDirections.Add(direction);
                potentialCounts.Add(count);
                bestCount = (count > bestCount) ? count : bestCount;
            }
        }

        Vector3 pickedDirection = Vector3.zero;
        if(potentialDirections.Count > 0)
        {
            bool directionPicked = false;
            while (directionPicked == false)
            {
                int i = Random.Range(0, potentialDirections.Count);
                if (potentialCounts[i] >= Mathf.FloorToInt(bestCount / 2f) + 1)
                {
                    pickedDirection = potentialDirections[i];
                    directionPicked = true;
                }
            }
        }
        else
        {
            Debug.LogWarning("No good move direction");
        }

        return pickedDirection;
    }

    public static Vector2 GetDestroyPosition(RobotPawn pawn)
    {
        // Copy current grid to manipulate it
        int[,] newGrid = new int[TerrainManager.Instance.Grid.GetLength(0), TerrainManager.Instance.Grid.GetLength(1)];
        for (int z = 0; z < TerrainManager.Instance.Grid.GetLength(1); z++)
        {
            for (int x = 0; x < TerrainManager.Instance.Grid.GetLength(0); x++)
            {
                newGrid[x, z] = TerrainManager.Instance.Grid[x, z];
            }
        }

        List<Vector2> positions = new List<Vector2>();
        for (int z = 0; z < newGrid.GetLength(1); z++)
        {
            for(int x = 0; x < newGrid.GetLength(0); x++)
            {
                if(newGrid[x, z] == 0)
                {
                    int currentScore = 0;
                    foreach(Vector3 direction in m_Directions)
                    {
                        int xx = (int)(x + direction.x);
                        int zz = (int)(z + direction.z);
                        if (TerrainManager.Instance.IsInsideBounds(xx, zz) && newGrid[xx, zz] > 0)
                        {
                            if(Mathf.FloorToInt(newGrid[xx, zz] / 10) != pawn.Player.Index)
                            {
                                currentScore += 5;
                            }
                            else if (Mathf.FloorToInt(newGrid[xx, zz] / 10) == pawn.Player.Index)
                            {
                                currentScore -= 5;
                            }
                            else if (newGrid[xx, zz] == -1)
                            {
                                currentScore += 1;
                            }
                        }
                    }

                    if(currentScore > 0)
                    {
                        positions.Add(new Vector2(x, z));
                    }
                }
            }
        }

        Vector2 position = new Vector2(-1, -1);
        if(positions.Count > 0)
        {
            position = positions[Random.Range(0, positions.Count)];
        }
        else
        {
            bool validPosition = false;
            while (validPosition == false)
            {
                int x = Random.Range(0, newGrid.GetLength(0));
                int z = Random.Range(0, newGrid.GetLength(1));

                if (TerrainManager.Instance.GetCellValue(new Vector2(x, z)) == 0)
                {
                    position = new Vector2(x, z);
                    validPosition = true;
                }
            }
        }
        return position;
    }

    public static TerrainChunk GetTerrainChunkAndDirection(RobotPawn pawn, out Vector3 direction)
    {
        // Copy current grid to manipulate it
        int[,] newGrid = new int[TerrainManager.Instance.Grid.GetLength(0), TerrainManager.Instance.Grid.GetLength(1)];
        for (int z = 0; z < TerrainManager.Instance.Grid.GetLength(1); z++)
        {
            for (int x = 0; x < TerrainManager.Instance.Grid.GetLength(0); x++)
            {
                newGrid[x, z] = TerrainManager.Instance.Grid[x, z];
            }
        }
        List<TerrainChunk> chunks = new List<TerrainChunk>();
        List<Vector3> directions = new List<Vector3>();
        List<int> scores = new List<int>();
        int bestScore = 0;

        // Compute score for all possible moves
        for (int z = 0; z < newGrid.GetLength(1); z++)
        {
            for (int x = 0; x < newGrid.GetLength(0); x++)
            {
                if(newGrid[x, z] >= 0)
                {
                    TerrainChunk chunk = TerrainManager.Instance.GetTerrainChunk(x, z);
                    if(chunk != null && TerrainManager.Instance.CheckLocalTerrainMoveFeasibility(x, z) && TerrainManager.Instance.HasAlreadyMoved(chunk.gameObject) == false)
                    {
                        if (newGrid[x, z] == 0) // if the moved chunk is empty
                        {
                            int currentScore = 0;
                            foreach (Vector3 dir in m_Directions)
                            {
                                int xx = (int)(x + dir.x);
                                int zz = (int)(z + dir.z);
                                if (TerrainManager.Instance.IsInsideBounds(xx, zz) && newGrid[xx, zz] == -1)
                                {
                                    // Is the empty chunk gonna give a walk solution when moved ?
                                    foreach(Vector3 d in m_Directions)
                                    {
                                        int xxx = (int)(xx + d.x);
                                        int zzz = (int)(zz + d.z);
                                        if(TerrainManager.Instance.IsInsideBounds(xxx, zzz) && xxx != x && zzz != z)
                                        {
                                            if(Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) == pawn.Player.Index)
                                            {
                                                currentScore += 4;
                                            }
                                            else if(Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) != pawn.Player.Index)
                                            {
                                                currentScore -= 4;
                                            }
                                        }
                                    }

                                    // Is the empty chunk gonna give a jump solution when moved ?
                                    foreach (Vector3 d in m_Directions)
                                    {
                                        int xxx = (int)(xx + 2 * d.x);
                                        int zzz = (int)(zz + 2 * d.z);
                                        if(TerrainManager.Instance.IsInsideBounds(xxx, zzz))
                                        {
                                            if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) == pawn.Player.Index)
                                            {
                                                currentScore += 2;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) != pawn.Player.Index)
                                            {
                                                currentScore -= 2;
                                            }
                                        }
                                    }

                                    if (currentScore > 0)
                                    {
                                        chunks.Add(chunk);
                                        directions.Add(dir);
                                        scores.Add(currentScore);
                                    }

                                    if(currentScore > bestScore)
                                    {
                                        bestScore = currentScore;
                                    }
                                }
                            }

                        }
                        else if (Mathf.FloorToInt(newGrid[x, z] / 10f) == pawn.Player.Index)
                        {
                            int currentScore = 0;
                            foreach (Vector3 dir in m_Directions)
                            {
                                int xx = (int)(x + dir.x);
                                int zz = (int)(z + dir.z);
                                if (TerrainManager.Instance.IsInsideBounds(xx, zz) && newGrid[xx, zz] == -1)
                                {
                                    // Is the chunk carrying one of the robot player's chunk gonna give a walk solution when moved ?
                                    foreach (Vector3 d in m_Directions)
                                    {
                                        int xxx = (int)(xx + d.x);
                                        int zzz = (int)(zz + d.z);
                                        if (TerrainManager.Instance.IsInsideBounds(xxx, zzz) && xxx != x && zzz != z)
                                        {
                                            if (newGrid[xxx, zzz] == 0)  // potentially adding a walk option to its pawn
                                            {
                                                currentScore += 4;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) == pawn.Player.Index) // potentially blocking one of it's pawns to walk
                                            {
                                                currentScore -= 2;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) != pawn.Player.Index) // potentially blocking one of opponent's pawns to walk
                                            {
                                                currentScore += 2;
                                            }
                                            else if (newGrid[xxx, zzz] == -1)
                                            {
                                                currentScore -= 4;
                                            }
                                        }
                                    }

                                    // Is the empty chunk gonna give a jump solution when moved ?
                                    foreach (Vector3 d in m_Directions)
                                    {
                                        int xxx = (int)(xx + 2 * d.x);
                                        int zzz = (int)(zz + 2 * d.z);
                                        if(TerrainManager.Instance.IsInsideBounds(xxx, zzz))
                                        {
                                            if (newGrid[xxx, zzz] == 0)  // potentially adding a walk option to its pawn
                                            {
                                                currentScore += 2;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) == pawn.Player.Index) // potentially blocking one of it's pawns to walk
                                            {
                                                currentScore -= 1;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) != pawn.Player.Index) // potentially blocking one of opponent's pawns to walk
                                            {
                                                currentScore += 1;
                                            }
                                            else if (newGrid[xxx, zzz] == -1)
                                            {
                                                currentScore -= 2;
                                            }
                                        }
                                    }

                                    if (currentScore > 0)
                                    {
                                        chunks.Add(chunk);
                                        directions.Add(dir);
                                        scores.Add(currentScore);
                                    }

                                    if (currentScore > bestScore)
                                    {
                                        bestScore = currentScore;
                                    }
                                }
                            }
                        }
                        else if (Mathf.FloorToInt(newGrid[x, z] / 10f) != pawn.Player.Index) // moving a opponent turtle with the chunk
                        {
                            int currentScore = 0;
                            foreach (Vector3 dir in m_Directions)
                            {
                                int xx = (int)(x + dir.x);
                                int zz = (int)(z + dir.z);
                                if (TerrainManager.Instance.IsInsideBounds(xx, zz) && newGrid[xx, zz] == -1)
                                {
                                    // Is the chunk carrying one of the robot opponent's chunk gonna give a walk solution when moved ?
                                    foreach (Vector3 d in m_Directions)
                                    {
                                        int xxx = (int)(xx + d.x);
                                        int zzz = (int)(zz + d.z);
                                        if (TerrainManager.Instance.IsInsideBounds(xxx, zzz) && xxx != x && zzz != z)
                                        {
                                            if (newGrid[xxx, zzz] == 0)  // potentially adding a walk option to opponenent pawn
                                            {
                                                currentScore -= 4;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) == pawn.Player.Index) // potentially blocking opponent's pawns to walk
                                            {
                                                currentScore -= 2;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) != pawn.Player.Index) // potentially blocking one of it's pawns to walk
                                            {
                                                currentScore += 2;
                                            }
                                            else if (newGrid[xxx, zzz] == -1) // moving toward an empty slot
                                            {
                                                currentScore += 4;
                                            }
                                        }
                                    }

                                    // Is the empty chunk gonna give a jump solution when moved ?
                                    foreach (Vector3 d in m_Directions)
                                    {
                                        int xxx = (int)(xx + 2 * d.x);
                                        int zzz = (int)(zz + 2 * d.z);
                                        if(TerrainManager.Instance.IsInsideBounds(xxx, zzz))
                                        {
                                            if (newGrid[xxx, zzz] == 0)
                                            {
                                                currentScore -= 2;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) == pawn.Player.Index)
                                            {
                                                currentScore -= 1;
                                            }
                                            else if (newGrid[xxx, zzz] > 0 && Mathf.FloorToInt(newGrid[xxx, zzz] / 10f) != pawn.Player.Index)
                                            {
                                                currentScore += 1;
                                            }
                                            else if (newGrid[xxx, zzz] == -1)
                                            {
                                                currentScore += 2;
                                            }
                                        }
                                    }

                                    if (currentScore > 0)
                                    {
                                        chunks.Add(chunk);
                                        directions.Add(dir);
                                        scores.Add(currentScore);
                                    }

                                    if (currentScore > bestScore)
                                    {
                                        bestScore = currentScore;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Pick a move
        TerrainChunk pickedChunk = null;
        direction = Vector3.zero;
        if (chunks.Count > 0)
        {
            bool chunkPicked = false;
            while (chunkPicked == false)
            {
                int i = Random.Range(0, chunks.Count);
                if (scores[i] >= Mathf.FloorToInt(bestScore / 2f) + 1)
                {
                    pickedChunk = chunks[i];
                    direction = directions[i];
                    chunkPicked = true;
                }
            }
        }
        else // if no "good" moves, pick one randomly
        {
            bool randomChunkPicked = false;
            while(randomChunkPicked == false)
            {
                int x = Random.Range(0, newGrid.GetLength(0));
                int z = Random.Range(0, newGrid.GetLength(1));
                TerrainChunk randomChunk = TerrainManager.Instance.GetTerrainChunk(x, z);

                if(randomChunk != null && TerrainManager.Instance.CheckLocalTerrainMoveFeasibility(x, z) && TerrainManager.Instance.HasAlreadyMoved(randomChunk.gameObject) == false)
                {
                    foreach(Vector3 dir in m_Directions)
                    {
                        int xx = (int)(x + dir.x);
                        int zz = (int)(z + dir.z);
                        if(randomChunkPicked == false && TerrainManager.Instance.IsInsideBounds(xx, zz) && newGrid[xx, zz] == -1)
                        {
                            pickedChunk = randomChunk;
                            direction = dir;
                            randomChunkPicked = true;
                        }
                    }
                }
            }
        }

        return pickedChunk;
    }
}