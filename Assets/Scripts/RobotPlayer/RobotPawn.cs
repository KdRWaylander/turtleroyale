﻿using System.Collections;
using UnityEngine;

public class RobotPawn : Pawn
{
    public new Player Player { get { return m_RobotPlayer; } }
    public new int Index { get; private set; }

    private Player m_RobotPlayer;
    private Animator m_RobotAnimator;
    private RobotPawnColor m_RobotPawnColor;
    private RobotPawnMovement m_RobotPawnMovement;
    private RobotPawnActionDestroy m_RobotPawnActionDestroy;
    private RobotPawnActionTerrain m_RobotPawnActionTerrain;

    private GameObject m_RobotBelt;

    private void Awake()
    {
        m_RobotPlayer = GetComponentInParent<Player>();
        m_RobotAnimator = GetComponentInChildren<Animator>();
        m_RobotPawnColor = GetComponent<RobotPawnColor>();
        m_RobotPawnMovement = GetComponent<RobotPawnMovement>();
        m_RobotPawnActionDestroy = GetComponent<RobotPawnActionDestroy>();
        m_RobotPawnActionTerrain = GetComponent<RobotPawnActionTerrain>();
        m_RobotBelt = GetComponentInChildren<Belt>().gameObject;
    }

    private void Start()
    {
        Index = transform.parent.childCount;
        name = "Pawn " + Index;

        NumberOfPlayedActions = 0;
        HasPlayed = false;

        m_RobotBelt.GetComponent<MeshRenderer>().material.color = FindObjectOfType<PlayerColorManager>().PlayerColors[GetComponentInParent<Player>().Index - 1];
        ToggleBelt(false);

        TerrainManager.Instance.SetCellValue(new Vector2(transform.position.x, transform.position.z), Player.Index * 10 + Index);
    }

    private void Update()
    {
        if (IsTurn && NumberOfPlayedActions < GameManager.Instance.MaxNumberOfPlayedActions)
        {
            // Check if player can do its action. If not he loses. Exception for terrain movement, the action is only skipped
            if (TerrainManager.Instance.CheckActionFeasibility(transform.position, GameManager.Instance.Action) == false)
            {
                if (GameManager.Instance.Action == TurnAction.Move || GameManager.Instance.Action == TurnAction.Jump)
                {
                    StartCoroutine(RemovePawn());
                }
                else
                {
                    EndPawnTurn();
                }
                return;
            }

            // If action is possible
            ToggleBelt(true);
            Action();
        }

        if (HasPlayed)
        {
            EndPawnTurn();
        }
    }

    private IEnumerator RemovePawn()
    {
        ToggleBelt(true);
        m_RobotAnimator.SetTrigger("Death");
        yield return new WaitForSeconds(1.3f);
        SettingsManager.Instance.PlayPawnDeathSound();
        PlayerManager.Instance.RemoveRobotPawn(this);
    }

    public new void Action()
    {
        if (GameManager.Instance.Action == TurnAction.Move)
        {
            m_RobotPawnMovement.Move();
        }
        else if (GameManager.Instance.Action == TurnAction.Jump)
        {
            m_RobotPawnMovement.Jump();
        }
        else if (GameManager.Instance.Action == TurnAction.Destroy)
        {
            m_RobotPawnActionDestroy.Destroy();
        }
        else if (GameManager.Instance.Action == TurnAction.Terrain)
        {
            m_RobotPawnActionTerrain.Move();
        }
    }

    public new void ToggleBelt(bool toggle)
    {
        m_RobotBelt.SetActive(toggle);
    }

    public new void EndPawnTurn()
    {
        ToggleBelt(false);

        NumberOfPlayedActions = 0;
        IsTurn = false;
        HasPlayed = true;
    }
}