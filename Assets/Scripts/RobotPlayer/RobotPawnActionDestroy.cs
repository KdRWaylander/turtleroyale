﻿using System.Collections;
using UnityEngine;

public class RobotPawnActionDestroy : MonoBehaviour
{
    private bool m_IsDestroying = false;
    private RobotPawn m_RobotPawn;

    private void Awake()
    {
        m_RobotPawn = GetComponent<RobotPawn>();
    }

    public void Destroy()
    {
        if(m_IsDestroying == false)
        {
            StartCoroutine(Destroyment());
        }
    }

    private IEnumerator Destroyment()
    {
        Vector2 hitPosition = RobotBrain.GetDestroyPosition(m_RobotPawn);
        m_IsDestroying = true;

        yield return new WaitForSeconds(1f);

        /*
        Vector2 hitPosition = Vector2.zero;
        bool validPosition = false;
        while (validPosition == false)
        {
            int x = Random.Range(0, 6);
            int z = Random.Range(0, 6);

            if (TerrainManager.Instance.GetCellValue(new Vector2(x, z)) == 0)
            {
                hitPosition = new Vector2(x, z);
                validPosition = true;
            }
        }

        Vector3 hitPosition = MinimaxManager.Instance.Destroy(m_RobotPawn);
        bool validPosition = false;
        while (validPosition == false)
        {
            if (TerrainManager.Instance.GetCellValue(new Vector2(hitPosition.x, hitPosition.z)) == 0)
            {
                validPosition = true;
            }
            else
            {
                hitPosition = MinimaxManager.Instance.Destroy(m_RobotPawn);
            }
        }
        */

        m_RobotPawn.NumberOfPlayedActions += 1;
        m_RobotPawn.ToggleBelt(false);

        SettingsManager.Instance.PlayDestroySound();
        TerrainManager.Instance.DestroyTerrainChunk(hitPosition);

        yield return new WaitForSeconds(1f);

        m_IsDestroying = false;

        m_RobotPawn.IsTurn = false;
        m_RobotPawn.HasPlayed = true;
    }
}