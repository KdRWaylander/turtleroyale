﻿using System.Collections;
using UnityEngine;

public class RobotPawnActionTerrain : MonoBehaviour
{
    private bool m_IsMoving = false;
    private TerrainChunk m_SelectedTerrainChunk;

    private RobotPawn m_RobotPawn;
    private RobotPawnMovement m_RobotPawnMovement;

    private void Awake()
    {
        m_RobotPawn = GetComponent<RobotPawn>();
        m_RobotPawnMovement = GetComponent<RobotPawnMovement>();
    }

    private void Start()
    {
        m_SelectedTerrainChunk = null;
    }

    public void Move()
    {
        if(m_IsMoving == false)
        {
            StartCoroutine(Movement());
        }
    }

    private IEnumerator Movement()
    {
        /*
        m_IsMoving = true;

        yield return new WaitForSeconds(1f);

        TerrainChunk[] terrainChunks = FindObjectsOfType<TerrainChunk>();
        foreach(TerrainChunk terrainChunk in terrainChunks)
        {
            if (TerrainManager.Instance.HasAlreadyMoved(terrainChunk.transform.gameObject) == false && TerrainManager.Instance.CheckLocalTerrainMoveFeasibility(terrainChunk.transform.position))
            {
                m_SelectedTerrainChunk = terrainChunk;
                SettingsManager.Instance.PlayTerrainSelectionSound();
                break;
            }
        }
        m_SelectedTerrainChunk.Highlight(true, m_RobotPawn.GetComponent<RobotPawnColor>().BaseColor);
        yield return new WaitForSeconds(1f);

        bool directionValid = false;
        while(directionValid == false)
        {
            var direction = m_RobotPawnMovement.GetRandomDirection();
            var newPos = new Vector3(m_SelectedTerrainChunk.transform.position.x + direction.x, 0, m_SelectedTerrainChunk.transform.position.z + direction.z);

            //Vector3 direction = Vector3.zero;
            //Vector3 newPos = MinimaxManager.Instance.Terrain(m_RobotPawn, out direction);

            if (TerrainManager.Instance.IsInsideBounds((int)newPos.x, (int)newPos.z) == true && TerrainManager.Instance.CheckMovementValidity(new Vector2(newPos.x, newPos.z)))
            {
                directionValid = true;
                m_RobotPawn.NumberOfPlayedActions += 1;
                m_RobotPawn.ToggleBelt(false);

                SettingsManager.Instance.PlayTerrainMovementSound();
                if (TerrainManager.Instance.GetCellValue(new Vector2(m_SelectedTerrainChunk.transform.position.x, m_SelectedTerrainChunk.transform.position.z)) > 0)
                {
                    Vector3 chunkPosition = m_SelectedTerrainChunk.transform.position;
                    TerrainManager.Instance.MoveTerrainChunk(m_SelectedTerrainChunk.gameObject, direction);

                    bool hasMove = false;
                    PawnMovement[] pawns = FindObjectsOfType<PawnMovement>();
                    for (int i = 0; i < pawns.Length; i++)
                    {
                        if (pawns[i].transform.position.x == chunkPosition.x && pawns[i].transform.position.z == chunkPosition.z)
                        {
                            pawns[i].Move(direction);
                            hasMove = true;
                            break;
                        }
                    }
                    if (hasMove == false)
                    {
                        RobotPawnMovement[] robotPawns = FindObjectsOfType<RobotPawnMovement>();
                        for (int i = 0; i < robotPawns.Length; i++)
                        {
                            if (robotPawns[i].transform.position.x == chunkPosition.x && robotPawns[i].transform.position.z == chunkPosition.z)
                            {
                                robotPawns[i].Move(direction);
                                hasMove = true;
                                break;
                            }
                        }

                    }
                }
                else
                {
                    TerrainManager.Instance.MoveTerrainChunk(m_SelectedTerrainChunk.gameObject, direction);
                }

                m_SelectedTerrainChunk.Highlight(false, Color.white);
                m_SelectedTerrainChunk = null;
                m_IsMoving = false;

                m_RobotPawn.IsTurn = false;
                m_RobotPawn.HasPlayed = true;
            }
        }
        */

        Vector3 direction = Vector3.zero;
        m_SelectedTerrainChunk = RobotBrain.GetTerrainChunkAndDirection(m_RobotPawn, out direction);
        m_IsMoving = true;

        yield return new WaitForSeconds(1f);
        
        m_SelectedTerrainChunk.Highlight(true, m_RobotPawn.GetComponent<RobotPawnColor>().BaseColor);
        
        yield return new WaitForSeconds(1f);

        m_RobotPawn.NumberOfPlayedActions += 1;
        m_RobotPawn.ToggleBelt(false);

        SettingsManager.Instance.PlayTerrainMovementSound();
        if (TerrainManager.Instance.GetCellValue(new Vector2(m_SelectedTerrainChunk.transform.position.x, m_SelectedTerrainChunk.transform.position.z)) > 0)
        {
            Vector3 chunkPosition = m_SelectedTerrainChunk.transform.position;
            TerrainManager.Instance.MoveTerrainChunk(m_SelectedTerrainChunk.gameObject, direction);

            bool hasMoved = false;
            PawnMovement[] pawns = FindObjectsOfType<PawnMovement>();
            for (int i = 0; i < pawns.Length; i++)
            {
                if (pawns[i].transform.position.x == chunkPosition.x && pawns[i].transform.position.z == chunkPosition.z)
                {
                    pawns[i].Move(direction);
                    hasMoved = true;
                    break;
                }
            }
            if (hasMoved == false)
            {
                RobotPawnMovement[] robotPawns = FindObjectsOfType<RobotPawnMovement>();
                for (int i = 0; i < robotPawns.Length; i++)
                {
                    if (robotPawns[i].transform.position.x == chunkPosition.x && robotPawns[i].transform.position.z == chunkPosition.z)
                    {
                        robotPawns[i].Move(direction);
                        hasMoved = true;
                        break;
                    }
                }

            }
        }
        else
        {
            TerrainManager.Instance.MoveTerrainChunk(m_SelectedTerrainChunk.gameObject, direction);
        }

        m_SelectedTerrainChunk.Highlight(false, Color.white);
        m_SelectedTerrainChunk = null;
        m_IsMoving = false;

        m_RobotPawn.IsTurn = false;
        m_RobotPawn.HasPlayed = true;
    }
}