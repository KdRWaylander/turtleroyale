﻿using System.Collections;
using UnityEngine;

public class RobotPawnMovement : MonoBehaviour
{
    private bool m_IsMoving = false;

    private RobotPawn m_RobotPawn;
    private TurtleAnimator m_TurtleAnimator;

    private void Awake()
    {
        m_RobotPawn = GetComponent<RobotPawn>();
        m_TurtleAnimator = GetComponentInChildren<TurtleAnimator>();
    }

    public void Move(Vector3 direction)
    {
        transform.position += direction;
        TerrainManager.Instance.SetCellValue(new Vector2(transform.position.x, transform.position.z), m_RobotPawn.Player.Index * 10 + m_RobotPawn.Index);
    }

    public void Move()
    {
        if(m_IsMoving == false)
        {
            StartCoroutine(Movement(1));
        }
    }

    public void Jump()
    {
        if (m_IsMoving == false)
        {
            StartCoroutine(Movement(2));
        }
    }

    public Vector3 GetRandomDirection()
    {
        float rng = Random.value;
        if(rng <= .25f)
        {
            return Vector3.forward;
        }
        else if (rng > .25f && rng <= .5f)
        {
            return Vector3.back;
        }
        else if (rng > .5f && rng <= .75f)
        {
            return Vector3.right;
        }
        else
        {
            return Vector3.left;
        }
    }

    private IEnumerator Movement(int length)
    {
        Vector3 direction = length * RobotBrain.GetDirection(m_RobotPawn, length);
        m_IsMoving = true;

        yield return new WaitForSeconds(1f);
        
        /*
        Vector3 direction = Vector3.zero;
        bool isValid = false;
        while(isValid == false)
        {
            direction = length * GetRandomDirection();
            isValid = TerrainManager.Instance.CheckPositionValidity(transform.position, direction, length);
        }
        //Vector3 direction = MinimaxManager.Instance.Direction(m_RobotPawn, length);
        */


        m_RobotPawn.NumberOfPlayedActions += 1;
        m_RobotPawn.ToggleBelt(false);
        transform.forward = direction.normalized;

        if (length == 1)
        {
            Vector3 destination = transform.position + direction;

            SettingsManager.Instance.PlayMovementSound();
            m_TurtleAnimator.TriggerMoveAnimation();

            float duration = 1.5f;
            float elapsedTime = 0f;
            float speed = .65f;

            while (elapsedTime < duration)
            {
                transform.position += direction * Time.deltaTime * speed;
                yield return null;
                elapsedTime += Time.deltaTime;
            }

            transform.position = destination;
            m_TurtleAnimator.TriggerIdleAnimation();
        }
        else
        {
            m_TurtleAnimator.TriggerRetreatAnimation();

            yield return new WaitForSeconds(1.2f);

            SettingsManager.Instance.PlayTeleportSound();
            transform.position += direction;
            m_TurtleAnimator.TriggerRetreatAnimation();

            yield return new WaitForSeconds(1.2f);
        }

        TerrainManager.Instance.SetCellValue(new Vector2(transform.position.x, transform.position.z), m_RobotPawn.Player.Index * 10 + m_RobotPawn.Index);
        TerrainManager.Instance.SetCellValue(new Vector2((transform.position - direction).x, (transform.position - direction).z), 0);

        m_IsMoving = false;

        m_RobotPawn.IsTurn = false;
        m_RobotPawn.HasPlayed = true;
    }
}