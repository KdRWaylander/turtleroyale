﻿using UnityEngine;

public class Colorizer : MonoBehaviour
{
    [SerializeField] Gradient m_ColorGradient;

    private void Start()
    {
        Color color = m_ColorGradient.Evaluate(Random.value);
        GetComponent<MeshRenderer>().material.color = color;
    }
}