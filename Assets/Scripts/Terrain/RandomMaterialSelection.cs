﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMaterialSelection : MonoBehaviour
{
    [SerializeField] private Material[] m_Materials;

    private MeshRenderer m_MeshRenderer;

    private void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        int i = Random.Range(0, m_Materials.Length);
        Material mat = m_Materials[i];

        m_MeshRenderer.material = mat;
    }
}