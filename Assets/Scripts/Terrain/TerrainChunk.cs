﻿using UnityEngine;

public class TerrainChunk : MonoBehaviour
{
    private MeshRenderer m_Renderer;

    private void Awake()
    {
        m_Renderer = GetComponent<MeshRenderer>();
    }

    public void Highlight(bool isToBeHighlighted, Color baseColor)
    {
        if (isToBeHighlighted)
        {
            m_Renderer.material.color = Color.Lerp(baseColor, Color.black, .5f);
        }
        else
        {
            m_Renderer.material.color = baseColor;
        }
    }
}