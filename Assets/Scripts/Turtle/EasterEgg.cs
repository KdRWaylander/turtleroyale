﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasterEgg : MonoBehaviour
{
    [SerializeField] private int m_TriggerNumber;
    [SerializeField] private float m_MaxTime;
    private bool m_IsRunning = false;

    private AudioSource m_AudioSource;

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                && TerrainManager.Instance.GetCellValue(new Vector2(hit.transform.position.x, hit.transform.position.z)) == 1
                && m_IsRunning == false)
            {
                StartCoroutine(GoatSound(m_TriggerNumber, m_MaxTime));
            }
        }
        #else
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                && TerrainManager.Instance.GetCellValue(new Vector2(hit.transform.position.x, hit.transform.position.z)) == 1
                && m_IsRunning == false)
            {
                StartCoroutine(GoatSound(m_TriggerNumber, m_MaxTime));
            }
            }
        }
        #endif
    }

    private IEnumerator GoatSound(int trigger, float maxTime)
    {
        m_IsRunning = true;
        int clicks = 0;
        float elapsed = 0f;

        while (m_IsRunning)
        {
            RaycastHit hit;

            #if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Mouse0)
                && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                && TerrainManager.Instance.GetCellValue(new Vector2(hit.transform.position.x, hit.transform.position.z)) == 1)
            {
                clicks += 1;
                elapsed = 0;

                if (clicks >= 10)
                    m_IsRunning = false;
            }
            #else
            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began
                    && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)
                    && TerrainManager.Instance.GetCellValue(new Vector2(hit.transform.position.x, hit.transform.position.z)) == 1)
                {
                    clicks += 1;
                    elapsed = 0;

                    if (clicks >= 10)
                        m_IsRunning = false;
                }
            }
            #endif

            yield return null;

            elapsed += Time.deltaTime;
            if (elapsed >= maxTime)
                m_IsRunning = false;
        }

        if (clicks >= trigger)
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
    }
}