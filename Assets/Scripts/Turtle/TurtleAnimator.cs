﻿using UnityEngine;

public class TurtleAnimator : MonoBehaviour
{
    private Animator m_Animator;

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    public void TriggerRetreatAnimation()
    {
        m_Animator.SetTrigger("Retreat");
    }

    public void TriggerMoveAnimation()
    {
        m_Animator.SetTrigger("Move");
    }

    public void TriggerIdleAnimation()
    {
        m_Animator.SetTrigger("Idle");
    }
}