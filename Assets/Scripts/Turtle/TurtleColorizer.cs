﻿using UnityEngine;

public class TurtleColorizer : MonoBehaviour
{
    [SerializeField] private Color m_SkinColor;

    private SkinnedMeshRenderer m_TurtleRenderer;

    private void Awake()
    {
        m_TurtleRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        m_TurtleRenderer.material.EnableKeyword("_DETAIL_MULX2");
    }

    public void ChangeColor(Color shellColor)
    {
        m_TurtleRenderer.material.SetTexture("_MainTex", AlbedoDetailTexture(m_SkinColor, shellColor));
    }

    private Texture2D AlbedoDetailTexture(Color skinColor, Color shellColor)
    {
        Texture2D text2d = new Texture2D(128, 128);

        for (int i = 0; i < text2d.width; i++)
        {
            for (int j = 0; j < text2d.height; j++)
            {
                if(j < text2d.height / 2)
                {
                    text2d.SetPixel(i, j, shellColor);
                }
                else if(j >= text2d.height / 2)
                {
                    text2d.SetPixel(i, j, skinColor);
                }
            }
        }
        text2d.Apply();

        return text2d;
    }
}