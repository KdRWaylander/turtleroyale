﻿using UnityEngine;
using UnityEngine.UI;

public class UIAI : MonoBehaviour
{
    public bool AI { get; private set; }

    [SerializeField] private Sprite m_HumanSprite;
    [SerializeField] private Sprite m_AISprite;

    private Image m_ButtonImage;

    private void Awake()
    {
        m_ButtonImage = GetComponent<Image>();
    }

    private void Start()
    {
        AI = false;
        m_ButtonImage.sprite = m_HumanSprite;
    }

    public void SwitchAIStatus()
    {
        AI = !AI;
        m_ButtonImage.sprite = AI ? m_AISprite : m_HumanSprite;
    }
}