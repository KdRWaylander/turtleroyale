﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIActionImage : MonoBehaviour
{
    [Header("Sprites")]
    [SerializeField] private Sprite m_BackgroundSprite;
    [SerializeField] private Sprite m_TerrainActionBackgroundSprite;
    [SerializeField] private Sprite m_MoveActionBackgroundSprite;
    [SerializeField] private Sprite m_JumpActionBackgroundSprite;
    [SerializeField] private Sprite m_VictoryBackgroundSprite;
    [SerializeField] private Sprite m_VictorySprite;
    [SerializeField] private Sprite m_VictoryLaurelsSprite;
    [SerializeField] private Sprite m_DestroyActionSprite;
    [SerializeField] private Sprite m_PlaceActionSprite;
    [SerializeField] private Sprite m_MoveActionSprite;
    [SerializeField] private Sprite m_JumpActionSprite;
    [SerializeField] private Sprite m_TerrainActionSprite;

    [Header("Animators")]
    [SerializeField] private Animator m_ImageAnimator;
    [SerializeField] private Animator m_TextAnimator;

    private Image m_BackgroundImage;
    private Image m_ActionImage;

    private void Awake()
    {
        m_BackgroundImage = GetComponent<Image>();
        m_ActionImage = transform.GetChild(0).GetComponent<Image>();
    }

    void Start()
    {
        GameManager.Instance.NewActionDrawn += UpdateActionImage;
        GameManager.Instance.GameIsOver += VictoryImage;
    }

    public void UpdateActionImage(TurnAction action, Player player)
    {
        Color playerColor = FindObjectOfType<PlayerColorManager>().PlayerColors[player.Index - 1];
        switch (action)
        {
            case TurnAction.PlacePlayer:
                m_BackgroundImage.sprite = m_MoveActionBackgroundSprite;
                m_ActionImage.sprite = m_PlaceActionSprite;
                m_ActionImage.color = playerColor;
                break;
            case TurnAction.Move:
                m_BackgroundImage.sprite = m_MoveActionBackgroundSprite;
                m_ActionImage.sprite = m_MoveActionSprite;
                m_ActionImage.color = playerColor;
                break;
            case TurnAction.Jump:
                m_BackgroundImage.sprite = m_JumpActionBackgroundSprite;
                m_ActionImage.sprite = m_JumpActionSprite;
                m_ActionImage.color = playerColor;
                break;
            case TurnAction.Destroy:
                m_BackgroundImage.sprite = m_BackgroundSprite;
                m_ActionImage.sprite = m_DestroyActionSprite;
                m_ActionImage.color = Color.white;
                break;
            case TurnAction.Terrain:
                m_BackgroundImage.sprite = m_TerrainActionBackgroundSprite;
                m_ActionImage.sprite = m_TerrainActionSprite;
                m_ActionImage.color = playerColor;
                break;
            default:
                break;
        }
    }

    private void VictoryImage()
    {
        m_BackgroundImage.sprite = m_BackgroundSprite;
        m_ActionImage.sprite = m_VictoryLaurelsSprite;
        m_ActionImage.color = Color.white;

        StartCoroutine(Victory());
    }

    private IEnumerator Victory()
    {
        Time.timeScale = 1;
        m_ImageAnimator.SetTrigger("Victory");
        m_TextAnimator.SetTrigger("Victory");

        yield return new WaitForSeconds(1.75f);

        Time.timeScale = SettingsManager.Instance.GetGameSpeedFromPlayerPrefs();
        m_BackgroundImage.sprite = m_VictoryBackgroundSprite;
    }

    private void OnDestroy()
    {
        GameManager.Instance.NewActionDrawn -= UpdateActionImage;
        GameManager.Instance.GameIsOver -= VictoryImage;
    }
}