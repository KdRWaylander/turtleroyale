﻿using TMPro;
using UnityEngine;

public class UIActionText : MonoBehaviour
{
    private TextMeshProUGUI m_ActionText;

    private void Awake()
    {
        m_ActionText = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.Instance.NewActionDrawn += UpdateActionText;
    }

    private void UpdateActionText(TurnAction action, Player player)
    {
        switch (action)
        {
            case TurnAction.PlacePlayer:
                m_ActionText.text = LanguageManager.Instance.CurrentLanguage.Action_Place;
                break;
            case TurnAction.Move:
                m_ActionText.text = LanguageManager.Instance.CurrentLanguage.Action_Move;
                break;
            case TurnAction.Jump:
                m_ActionText.text = LanguageManager.Instance.CurrentLanguage.Action_Jump;
                break;
            case TurnAction.Destroy:
                m_ActionText.text = LanguageManager.Instance.CurrentLanguage.Action_Destroy;
                break;
            case TurnAction.Terrain:
                m_ActionText.text = LanguageManager.Instance.CurrentLanguage.Action_Terrain;
                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.NewActionDrawn -= UpdateActionText;
    }
}