﻿using System.Collections;
using UnityEngine;

public class UIBackButtonQuit : MonoBehaviour
{
    [SerializeField] private Animator m_ToastAnimator;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(QuitToast());
        }
    }

    private IEnumerator QuitToast()
    {
        m_ToastAnimator.gameObject.SetActive(true);
        m_ToastAnimator.SetTrigger("ToastIn");

        float elapsedTime = 0;
        while (elapsedTime < 3.0f)
        {
            yield return null;
            elapsedTime += Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        m_ToastAnimator.SetTrigger("ToastOut");
        yield return new WaitForSeconds(.2f);
        m_ToastAnimator.gameObject.SetActive(false);
    }
}