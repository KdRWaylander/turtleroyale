﻿using UnityEngine;
using UnityEngine.UI;

public class UIBackImage : MonoBehaviour
{
    private Image m_BackImage;

    private void Awake()
    {
        m_BackImage = GetComponent<Image>();
    }

    private void Start()
    {
        TurnManager.Instance.NewPlayerTurn += UpdateBackColor;
        GameManager.Instance.GameIsOver += VictoryBackColor;
    }

    public void UpdateBackColor(Player player)
    {
        m_BackImage.color = FindObjectOfType<PlayerColorManager>().PlayerColors[player.Index - 1];
    }

    private void VictoryBackColor()
    {
        m_BackImage.color = FindObjectOfType<PlayerColorManager>().PlayerColors[PlayerManager.Instance.GetPlayer(0).Index - 1];
    }

    private void OnDestroy()
    {
        TurnManager.Instance.NewPlayerTurn -= UpdateBackColor;
        GameManager.Instance.GameIsOver -= VictoryBackColor;
    }
}