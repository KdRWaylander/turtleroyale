﻿using UnityEngine;
using UnityEngine.UI;

public class UIBackgroundColor : MonoBehaviour
{
    private Image m_BackgroundImage;

    private void Awake()
    {
        m_BackgroundImage = GetComponent<Image>();
    }

    private void Start()
    {
        TurnManager.Instance.NewPlayerTurn += UpdateBackgroundColor;
        GameManager.Instance.GameIsOver += VictoryBackgroundColor;
    }

    public void UpdateBackgroundColor(Player player)
    {
        m_BackgroundImage.color = FindObjectOfType<PlayerColorManager>().PlayerColors[player.Index - 1];
    }

    private void VictoryBackgroundColor()
    {
        m_BackgroundImage.color = FindObjectOfType<PlayerColorManager>().PlayerColors[PlayerManager.Instance.GetPlayer(0).Index - 1];
    }

    private void OnDestroy()
    {
        TurnManager.Instance.NewPlayerTurn -= UpdateBackgroundColor;
        GameManager.Instance.GameIsOver -= VictoryBackgroundColor;
    }
}