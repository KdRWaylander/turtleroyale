﻿using TMPro;
using UnityEngine;

public class UICardsText : MonoBehaviour
{
    private TextMeshProUGUI m_CardsText;

    private void Awake()
    {
        m_CardsText = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        GameManager.Instance.NewActionDrawn += UpdateCardsText;
    }

    private void UpdateCardsText(TurnAction action, Player player)
    {
        string remainingActionsText = action == TurnAction.PlacePlayer ? 8.ToString() : (8 - GameManager.Instance.CurrentActionIndex - 1).ToString();
        m_CardsText.text = remainingActionsText + LanguageManager.Instance.CurrentLanguage.RemainingCards;
    }

    private void OnDestroy()
    {
        GameManager.Instance.NewActionDrawn -= UpdateCardsText;
    }
}