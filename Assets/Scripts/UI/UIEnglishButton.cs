﻿using UnityEngine;

public class UIEnglishButton : MonoBehaviour
{
    public void SetEnglishLanguage()
    {
        if (LanguageList.English != LanguageManager.Instance.CurrentLanguageType)
        {
            LanguageManager.Instance.SetNewLanguage(LanguageList.English);
        }
    }
}