﻿using UnityEngine;

public class UIFirstTimePanel : MonoBehaviour
{
    [SerializeField] private GameObject m_Panel;
    [SerializeField] private bool m_ResetFirstTime = true;

    private const string m_FirstTimeKey = "FIRSTTIME";

    private void Start()
    {
        if (m_ResetFirstTime)
        {
            PlayerPrefs.DeleteKey(m_FirstTimeKey);
            return;
        }

        if (PlayerPrefs.HasKey(m_FirstTimeKey) == false)
        {
            m_Panel.SetActive(true);
        }
    }

    public void Play()
    {
        PlayerPrefs.SetInt(m_FirstTimeKey, 1);
        m_Panel.SetActive(false);
    }
}