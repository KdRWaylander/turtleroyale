﻿using UnityEngine;

public class UIFrenchButton : MonoBehaviour
{
    public void SetFrenchLanguage()
    {
        if (LanguageList.French != LanguageManager.Instance.CurrentLanguageType)
        {
            LanguageManager.Instance.SetNewLanguage(LanguageList.French);
        }
    }
}