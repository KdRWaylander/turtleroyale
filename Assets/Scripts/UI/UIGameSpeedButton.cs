﻿using UnityEngine;

public class UIGameSpeedButton : MonoBehaviour
{
    [SerializeField] private GameSpeed m_GameSpeed;

    public void SetGameSpeed()
    {
        SettingsManager.Instance.SetGameSpeed(m_GameSpeed);
    }
}