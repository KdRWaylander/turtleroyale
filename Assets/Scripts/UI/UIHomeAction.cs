﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIHomeAction : MonoBehaviour
{
    public void Home()
    {
        SceneManager.LoadScene("MainMenu");
    }
}