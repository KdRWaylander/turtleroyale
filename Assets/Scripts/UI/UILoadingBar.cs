﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILoadingBar : MonoBehaviour
{
    [SerializeField] private string m_SceneToLoad;
    [SerializeField] private Image m_LoadingBarImage;
    [SerializeField] private UITogglePanel[] m_PanelsToClose;

    public void LoadLevel()
    {
        StartCoroutine(LoadAsynchronously());
    }

    private IEnumerator LoadAsynchronously()
    {
        foreach(UITogglePanel panel in m_PanelsToClose)
        {
            panel.Close();
        }

        AsyncOperation operation = SceneManager.LoadSceneAsync(m_SceneToLoad);

        if(TransferManager.Instance != null)
            TransferManager.Instance.CollectPlayerData();

        while (operation.isDone == false)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            m_LoadingBarImage.fillAmount = progress;

            yield return null;
        }
    }
}