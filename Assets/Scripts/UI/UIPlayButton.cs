﻿using UnityEngine;

public class UIPlayButton : MonoBehaviour
{
    [SerializeField] private UILoadingBar m_LoadingBar;

    public void Play()
    {
        m_LoadingBar.gameObject.SetActive(true);
        m_LoadingBar.LoadLevel();
    }
}