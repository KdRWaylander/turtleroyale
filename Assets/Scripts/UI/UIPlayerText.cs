﻿using UnityEngine;
using TMPro;

public class UIPlayerText : MonoBehaviour
{
    private TextMeshProUGUI m_PlayerText;

    private void Awake()
    {
        m_PlayerText = GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        TurnManager.Instance.NewPlayerTurn += UpdatePlayerText;
        GameManager.Instance.GameIsOver += VictoryText;
    }

    void UpdatePlayerText(Player player)
    {
        m_PlayerText.text = player.name;
    }

    void VictoryText()
    {
        m_PlayerText.text = PlayerManager.Instance.GetPlayer(0).name;
    }

    private void OnDestroy()
    {
        TurnManager.Instance.NewPlayerTurn -= UpdatePlayerText;
        GameManager.Instance.GameIsOver -= VictoryText;
    }
}