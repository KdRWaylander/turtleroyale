﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIRestartAction : MonoBehaviour
{
    public void Restart()
    {
        int index = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(index);
    }
}