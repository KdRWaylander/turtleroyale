﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISynchroObject : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Objects;

    public void CloseAllPanels(GameObject exception)
    {
        foreach (GameObject obj in m_Objects)
        {
            if (obj != exception)
            {
                obj.GetComponent<IToggle>().ToggleOff();
            }
        }
    }

    public void CloseAllPanels()
    {
        foreach (GameObject panel in m_Objects)
        {
            panel.GetComponent<IToggle>().ToggleOff();
        }
    }
}