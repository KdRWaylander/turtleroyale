﻿using UnityEngine;

public class UISyncroPanel : MonoBehaviour
{
    [SerializeField] private GameObject[] m_UIPanels;

    public void CloseAllPanels(GameObject exception)
    {
        foreach (GameObject panel in m_UIPanels)
        {
            if (panel != exception)
            {
                panel.GetComponent<IClose>().Close();
            }
        }
    }

    public void CloseAllPanels()
    {
        foreach (GameObject panel in m_UIPanels)
        {
            panel.GetComponent<IClose>().Close();
        }
    }
}