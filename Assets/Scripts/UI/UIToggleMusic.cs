﻿using UnityEngine;
using UnityEngine.UI;

public class UIToggleMusic : MonoBehaviour
{
    [SerializeField] private Sprite m_MusicOnSprite;
    [SerializeField] private Sprite m_MusicOffSprite;

    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        UpdateIcon();
    }

    public void Toggle()
    {
        bool isMute = SettingsManager.Instance.ToggleMusic();
        m_Image.sprite = isMute ? m_MusicOffSprite : m_MusicOnSprite;
    }

    public void UpdateIcon()
    {
        m_Image.sprite = SettingsManager.Instance.IsMusicMute ? m_MusicOffSprite : m_MusicOnSprite;
    }
}