﻿using UnityEngine;

public class UIToggleObject : MonoBehaviour, IToggle
{
    [SerializeField] private GameObject m_Object;
    [SerializeField] private bool m_CloseOnNewClick;
    
    public void ToggleOn()
    {
        if(m_Object.activeSelf == false)
        {
            GetComponentInParent<UISynchroObject>().CloseAllPanels(gameObject);
            m_Object.SetActive(true);
        }
        else
        {
            if(m_CloseOnNewClick == true)
            {
                m_Object.SetActive(false);
            }
        }
    }

    public void ToggleOff()
    {
        if(m_Object.activeSelf)
        {
            m_Object.SetActive(false);
        }
    }
}