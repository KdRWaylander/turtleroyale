﻿using UnityEngine;
using UnityEngine.UI;

public class UITogglePanel : MonoBehaviour, IClose
{
    public bool IsOpen { get { return m_Panel.GetComponent<RectTransform>().anchoredPosition.x > 0; } }

    [SerializeField] private GameObject m_Panel;
    [SerializeField] private float m_TranslationOffset;
    [SerializeField] private bool m_UseSyncroObject;

    private RectTransform m_RectTransform;

    private void Awake()
    {
        m_RectTransform = m_Panel.GetComponent<RectTransform>();
    }

    public void Open()
    {
        if (!IsOpen)
        {
            GetComponentInParent<UISyncroPanel>().CloseAllPanels(gameObject);
            GetComponentInParent<UISynchroObject>().CloseAllPanels();

            m_RectTransform.anchoredPosition += new Vector2(m_TranslationOffset, 0);
        }
        else
        {
            if(m_UseSyncroObject)
            {
                GetComponentInParent<UISynchroObject>().CloseAllPanels();
            }
        }
    }

    public void Close()
    {
        if (IsOpen)
        {
            m_RectTransform.anchoredPosition -= new Vector2(m_TranslationOffset, 0);
        }
    }
}