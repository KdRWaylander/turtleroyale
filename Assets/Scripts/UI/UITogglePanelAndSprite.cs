﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITogglePanelAndSprite : MonoBehaviour, IClose
{
    public bool IsOpen { get { return m_Panel.GetComponent<RectTransform>().anchoredPosition.x > 0; } }

    [SerializeField] private GameObject m_Panel;
    [SerializeField] private float m_TranslationOffset;

    [Header("Images")]
    [SerializeField] private Sprite m_RightArrow;
    [SerializeField] private Sprite m_LeftArrow;

    private Image m_Image;
    private RectTransform m_RectTransform;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
        m_RectTransform = m_Panel.GetComponent<RectTransform>();
    }

    public void Open()
    {
        if (!IsOpen)
        {
            GetComponentInParent<UISyncroPanel>().CloseAllPanels(gameObject);
            GetComponentInParent<UISynchroObject>().CloseAllPanels();

            m_RectTransform.anchoredPosition += new Vector2(m_TranslationOffset, 0);

            m_Image.sprite = m_RightArrow;
        }
        else
        {
            GetComponentInParent<UISynchroObject>().CloseAllPanels();
        }
    }

    public void Close()
    {
        if (IsOpen)
        {
            m_RectTransform.anchoredPosition -= new Vector2(m_TranslationOffset, 0);
            m_Image.sprite = m_LeftArrow;
        }
    }
}