﻿using UnityEngine;
using UnityEngine.UI;

public class UIToggleSettingsPanel : MonoBehaviour, IClose
{
    public bool IsOpen { get; private set; }

    [SerializeField] private GameObject m_Panel;
    [SerializeField] private float m_TranslationOffset;

    private RectTransform m_RectTransform;

    private void Awake()
    {
        m_RectTransform = m_Panel.GetComponent<RectTransform>();
    }

    private void Start()
    {
        IsOpen = false;
    }

    public void Toggle()
    {
        GetComponentInParent<UISyncroPanel>().CloseAllPanels(gameObject);

        IsOpen = !IsOpen;
        float multiplier = IsOpen ? 1 : -1;
        m_RectTransform.anchoredPosition += new Vector2(multiplier * m_TranslationOffset, 0);
    }

    public void Close()
    {
        if (IsOpen)
        {
            IsOpen = false;
            m_RectTransform.anchoredPosition -= new Vector2(m_TranslationOffset, 0);
        }
    }
}