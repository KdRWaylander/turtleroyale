﻿using UnityEngine;
using UnityEngine.UI;

public class UIToggleSound : MonoBehaviour
{
    [SerializeField] private Sprite m_SoundOnSprite;
    [SerializeField] private Sprite m_SoundOffSprite;

    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        UpdateIcon();
    }

    public void Toggle()
    {
        bool isMute = SettingsManager.Instance.ToggleSound();
        m_Image.sprite = isMute ? m_SoundOffSprite : m_SoundOnSprite;
    }

    public void UpdateIcon()
    {
        m_Image.sprite = SettingsManager.Instance.IsSFXMute ? m_SoundOffSprite : m_SoundOnSprite;
    }
}