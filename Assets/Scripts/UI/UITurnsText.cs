﻿using TMPro;
using UnityEngine;

public class UITurnsText : MonoBehaviour
{
    private TextMeshProUGUI m_TurnsText;
    private Animator m_Animator;

    private void Awake()
    {
        m_TurnsText = GetComponent<TextMeshProUGUI>();
        m_Animator = GetComponent<Animator>();
    }

    void Start()
    {
        TurnManager.Instance.NewTurn += UpdateTurnsText;
    }

    void UpdateTurnsText()
    {
        m_TurnsText.text = LanguageManager.Instance.CurrentLanguage.Action_Turn + TurnManager.Instance.Turns.ToString();
        m_Animator.SetTrigger("Turn");
    }

    private void OnDestroy()
    {
        TurnManager.Instance.NewTurn -= UpdateTurnsText;
    }
}