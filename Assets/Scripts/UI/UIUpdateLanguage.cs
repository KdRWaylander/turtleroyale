﻿using UnityEngine;
using TMPro;

public class UIUpdateLanguage : MonoBehaviour
{
    [SerializeField] private TextLine m_TextLine;

    private TextMeshProUGUI m_Text;

    private void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        UpdateText(LanguageManager.Instance.GetLanguageFromPlayerPrefs());
    }

    private void Start()
    {
        LanguageManager.Instance.NewLanguageSelected += UpdateText;
    }

    private void OnDisable()
    {
        LanguageManager.Instance.NewLanguageSelected -= UpdateText;
    }

    private void UpdateText(Language language)
    {
        switch (m_TextLine)
        {
            case TextLine.Configure_InputFieldPlaceHolder_1:
                m_Text.text = language.Configure_InputFieldPlaceHolder_1;
                break;
            case TextLine.Configure_InputFieldPlaceHolder_2:
                m_Text.text = language.Configure_InputFieldPlaceHolder_2;
                break;
            case TextLine.Configure_InputFieldPlaceHolder_3:
                m_Text.text = language.Configure_InputFieldPlaceHolder_3;
                break;
            case TextLine.Configure_InputFieldPlaceHolder_4:
                m_Text.text = language.Configure_InputFieldPlaceHolder_4;
                break;
            case TextLine.HowToPlay_Title:
                m_Text.text = language.HowToPlay_Title;
                break;
            case TextLine.m_GameRules:
                m_Text.text = language.m_GameRules;
                break;
            case TextLine.Settings_Title:
                m_Text.text = language.Settings_Title;
                break;
            case TextLine.Settings_Music:
                m_Text.text = language.Settings_Music;
                break;
            case TextLine.Settings_SFX:
                m_Text.text = language.Settings_SFX;
                break;
            case TextLine.Settings_Language:
                m_Text.text = language.Settings_Language;
                break;
            case TextLine.Credits_Title:
                m_Text.text = language.Credits_Title;
                break;
            case TextLine.Credits_Text:
                m_Text.text = language.Credits_Text;
                break;
            case TextLine.Button_Yes:
                m_Text.text = language.Button_Yes;
                break;
            case TextLine.Button_No:
                m_Text.text = language.Button_No;
                break;
            case TextLine.Button_HomeText:
                m_Text.text = language.Button_HomeText;
                break;
            case TextLine.Button_RestartText:
                m_Text.text = language.Button_RestartText;
                break;
            case TextLine.Button_QuitText:
                m_Text.text = language.Button_QuitText;
                break;
            case TextLine.Toast_Text:
                m_Text.text = language.Toast_Text;
                break;
            case TextLine.Tooltips_Play:
                m_Text.text = language.Tooltips_Play;
                break;
            case TextLine.Tooltips_Configure:
                m_Text.text = language.Tooltips_Configure;
                break;
            case TextLine.Tooltips_HowToPlay:
                m_Text.text = language.Tooltips_HowToPlay;
                break;
            case TextLine.Tooltips_Settings:
                m_Text.text = language.Tooltips_Settings;
                break;
            case TextLine.Tooltips_Credits:
                m_Text.text = language.Tooltips_Credits;
                break;
            case TextLine.Tooltips_Quit:
                m_Text.text = language.Tooltips_Quit;
                break;
            case TextLine.Tooltips_Color:
                m_Text.text = language.Tooltips_Color;
                break;
            case TextLine.Tooltips_ChangeNumberOfPlayers:
                m_Text.text = language.Tooltips_ChangeNumberOfPlayers;
                break;
            case TextLine.FirstTime_Title:
                m_Text.text = language.FirstTime_Title;
                break;
            case TextLine.FirstTime_Text:
                m_Text.text = language.FirstTime_Text;
                break;
            case TextLine.FirstTime_Language:
                m_Text.text = language.FirstTime_Language;
                break;
            case TextLine.FirstTime_Audio:
                m_Text.text = language.FirstTime_Audio;
                break;
            case TextLine.FirstTime_Play:
                m_Text.text = language.FirstTime_Play;
                break;
            case TextLine.Loading:
                m_Text.text = language.Loading;
                break;
            case TextLine.Tooltips_AI:
                m_Text.text = language.Tooltips_AI;
                break;
            case TextLine.GameSpeed_Title:
                m_Text.text = language.GameSpeed_Title;
                break;
            case TextLine.GameSpeed_Slow:
                m_Text.text = language.GameSpeed_Slow;
                break;
            case TextLine.GameSpeed_Normal:
                m_Text.text = language.GameSpeed_Normal;
                break;
            case TextLine.GameSpeed_Fast:
                m_Text.text = language.GameSpeed_Fast;
                break;
            default:
                m_Text.text = "";
                break;
        }
    }
}

[System.Serializable]
public enum TextLine
{
    Configure_InputFieldPlaceHolder_1,
    Configure_InputFieldPlaceHolder_2,
    Configure_InputFieldPlaceHolder_3,
    Configure_InputFieldPlaceHolder_4,
    HowToPlay_Title,
    m_GameRules,
    Settings_Title,
    Settings_Music,
    Settings_SFX,
    Settings_Language,
    Credits_Title,
    Credits_Text,
    Button_Yes,
    Button_No,
    Button_HomeText,
    Button_RestartText,
    Button_QuitText,
    Toast_Text,
    Tooltips_Play,
    Tooltips_Configure,
    Tooltips_HowToPlay,
    Tooltips_Settings,
    Tooltips_Credits,
    Tooltips_Quit,
    Tooltips_Color,
    Tooltips_ChangeNumberOfPlayers,
    FirstTime_Title,
    FirstTime_Text,
    FirstTime_Language,
    FirstTime_Audio,
    FirstTime_Play,
    Loading,
    Tooltips_AI,
    GameSpeed_Title,
    GameSpeed_Slow,
    GameSpeed_Normal,
    GameSpeed_Fast
}