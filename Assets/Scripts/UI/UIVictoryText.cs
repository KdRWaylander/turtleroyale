﻿using UnityEngine;
using TMPro;

public class UIVictoryText : MonoBehaviour
{
    private TextMeshProUGUI m_VictoryText;

    private void Awake()
    {
        m_VictoryText = GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        GameManager.Instance.GameIsOver += VictoryText;
    }

    void VictoryText()
    {
        m_VictoryText.text = LanguageManager.Instance.CurrentLanguage.Action_Victory;
    }

    private void OnDestroy()
    {
        GameManager.Instance.GameIsOver -= VictoryText;
    }
}